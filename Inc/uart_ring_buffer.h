
#ifndef _UART_RING__BUFFER_H
#define _UART_RING__BUFFER_H

#include "stdlib.h"
#include "main.h"

/* change the size of the buffer here */
#define UART1_BUFFER_SIZE 150
#define UART2_BUFFER_SIZE 16

typedef struct
{
  unsigned char buffer_uart1[UART1_BUFFER_SIZE];
  volatile unsigned int head_uart1;
  volatile unsigned int tail_uart1;
}ring_buffer_uart1;

typedef struct
{
  unsigned char buffer_uart2[UART2_BUFFER_SIZE];
  volatile unsigned int head_uart2;
  volatile unsigned int tail_uart2;
}ring_buffer_uart2;

extern volatile bool uart2RxCmplt;

/* Initialize the ring buffer */
void Ringbuf_Init(void);

/* Free buffer memory*/
extern void Ringbuf_UART1_Free(void);
extern void Ringbuf_UART2_Free(void);

/* once you hit 'enter' (\r\n), it copies the entire string to the buffer*/
extern void Get_UART1_String(char *buffer);
extern void Get_UART2_String(char *buffer);

/*After the predefined string is reached, it will copy the numberofchars after that into the buffertosave
 *USAGE---->>> if (Get_after("some string", 8, buffer)) { do something here}
 * */
//uint8_t Get_After(char *string, uint8_t numberofchars, char *buffertosave);

/* the ISR for the uart. put it in the IRQ handler */
extern void UART_ISR(UART_HandleTypeDef *huart);

//store uart string 
extern void Store_UART1_Char(unsigned char c, ring_buffer_uart1 *buffer);
extern void Store_UART2_Char(unsigned char c, ring_buffer_uart2 *buffer);

extern void UART2_HandleData(void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/