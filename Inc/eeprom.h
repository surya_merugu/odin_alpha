#ifndef __RE_EEPROM_H
#define __RE_EEPROM_H

#include "main.h"

#define EEPROM_WRITE 0x01
#define EEPROM_READ 0x02

#define EEPROM_RW_ADDR (uint16_t)0xAE
#define WAIT_TIME (uint8_t)0x06    /* Wait time = Internal write cycle time on eeprom */
#define MEM_ADD_SIZE (uint8_t)0x02 /* On 0x02 Master will write two bytes of memory addess after device address */

#define ODO_AT_START_OF_SWAP_ADDR (uint16_t)0x40
#define ODO_AT_START_OF_SWAP_LEN (uint8_t)0x04

#define EC20_RESET_CNT_ADDR   (uint16_t)0x00
#define EC20_RESET_CNT_LEN    (uint8_t)0x01

void RE_I2C1_Init(void);
void RE_EEPROM_Write(uint8_t *p_data, uint8_t len, uint16_t mem_address);
void RE_EEPROM_Read(uint8_t *p_data, uint8_t len, uint16_t mem_address);

#endif
