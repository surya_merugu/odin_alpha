#ifndef RE_EC20_H
#define RE_EC20_H

#include "main.h"

void RE_USART1_UART_Init(void);

#define UartRxQueueItems 12U
#define UartTxQueueItems 12U

#define UartRxQueueItemsLen 17U
#define UartTxQueueItemsLen 13U

void RE_EC20_PowerUp(void);
void RE_EC20_PowerDown(void);
void RE_EC20_Reset(void);
void RE_EC20_ReadMsg(uint8_t *p_Ec20RxMsg);
void RE_EC20_WriteMsg(uint8_t *p_Ec20TxMsg);

#endif