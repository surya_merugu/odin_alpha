#ifndef __RE_TASK_H
#define __RE_TASK_H

#include "stdbool.h"
#include "main.h"
#include "can.h"

#define EVT_QUEUE_LEN 5
#define EVT_QUEUE_ITEM_LEN 2
#define TASK_QUEUE_LEN 5
#define TASK_QUEUE_ITEM_LEN 2

bool RE_Task_Pending(void);
void RE_Create_EVT_Queue(void);
void RE_Create_TASK_Queue(void);
void RE_Load_SystemStatus(void);

#endif