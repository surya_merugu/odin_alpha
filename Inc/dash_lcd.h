/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef DASH_LCD_H
#define DASH_LCD_H

/* Includes */
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"

#define CMDOFF 0x00
#define CMDON 0x01
#define CMDLCDOFF 0x02
#define CMDLCDON 0x03
#define CMDB3C4 0x29

/**
  * @brief  Application Status structures definition
  */
typedef enum
{
    RE_OK = 0x00U,
    RE_ERROR = 0x01U,
    RE_BUSY = 0x02U,
    RE_TIMEOUT = 0x03U
} RE_StatusTypeDef;

/* Exported API's */
void RE_LCD_Init(void);

RE_StatusTypeDef RE_lcd_reset(void);
RE_StatusTypeDef RE_lcd_power_off(void);
RE_StatusTypeDef RE_lcd_write_data(uint8_t address, uint64_t data, uint16_t cs_pin);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/