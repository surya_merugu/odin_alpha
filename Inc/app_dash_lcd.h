/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_dash_lcd.h
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   FSTN LCD
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/******** Define to prevent recursive inclusion*****************************/
#ifndef APP_DASH_LCD_H
#define APP_DASH_LCD_H

/* Includes */
#include "main.h"
#include "dash_lcd.h"

/*------------------------------    Macros    -------------------------------*/
/* Equivalent hex value to display digit */
#define ZERO 0xBE
#define ONE 0x06
#define TWO 0x7C
#define THREE 0x5E
#define FOUR 0xC6
#define FIVE 0xDA
#define SIX 0xFA
#define SEVEN 0x0E
#define EIGHT 0xFE
#define NINE 0xDE

/* Equivalent hex value to dispaly segment */
#define LOW 0xB0
#define HIGH 0xE6
#define AUTO 0xEE

/* Dock fault representation */
#define DOCK_1 0x80
#define DOCK_2 0x04
#define DOCK_3 0x20
#define DOCK_4 0x02

/*Frigg Error codes to odin*/
#define G2G_ERROR_CODE (uint8_t)1
#define MISSING_PACKS_ERROR_CODE (uint8_t)2
#define TAMPER_ERROR_CODE (uint8_t)3
#define LATCH_ERROR_CODE (uint8_t)4
#define BK_BAT_LOW_ERROR_CODE (uint8_t)5
#define MOTOR_OVER_TEMP_ERROR_CODE (uint8_t)6
#define BAT_OVER_TEMP_ERROR_CODE (uint8_t)7
#define SPEED_SENSOR_ERROR_CODE (uint8_t)8

/* TypeDefs */
typedef struct
{
    uint64_t seg[2];
} CS_seg_t;

/* TN LCD Display Data */
typedef struct
{
    uint16_t err_code;
    uint8_t lock_status;

    uint8_t speed;
    uint8_t speed_graph;

    uint8_t gear;
    uint8_t gear_direction;

    uint8_t motor_temp;
    uint8_t motor_fault_graph;
    uint8_t kit_fault_graph;

    uint8_t bat_temp;
    uint16_t bat_volt;
    uint8_t bat_soc;
    uint8_t bat_soc_graph;
    uint8_t bat_fault_graph;

    uint8_t range;

    uint32_t odo;
    uint8_t odo_graph;
    uint8_t current_in_amp;
    uint16_t rpm_eff;
    uint8_t efficiency;
} FSTNLCD_data_t;

/* Exported API's */
extern FSTNLCD_data_t LcdDisplay;

RE_StatusTypeDef RE_RefreshScreen(void);
RE_StatusTypeDef RE_LCD_ON(void);

#endif
/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/