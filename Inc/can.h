#ifndef __RE_CAN_H
#define __RE_CAN_H

#include "main.h"
#include "queue.h"

#define Tx_NODE_ODIN (uint16_t)0x00C0

#define RX_NODE_ODIN_EEPROM 0x02
#define RX_NODE_BATTERY 0x03
#define RX_NODE_FRIGG 0x04

#define RxQueueItems 12U
#define TxQueueItems 8U
#define RxQueueItemLen 13U /* 4 byte id + 8 byte payload */
#define TxQueueItemLen 14U /* 4 byte id + 1 byte DLC + 8 byte payload */

void RE_CAN1_Init(void);
void RE_update_Bat_Data(void);
void RE_CAN_Format_TxMsg(uint8_t dest_node_id, uint8_t dock_id, uint8_t msg_id, uint8_t *p_data);
void RE_CAN_Read_Bat_Msg(uint8_t dock_id, uint8_t msg_id, uint8_t *p_data);
void RE_CAN_Read_Frigg_Msg(uint8_t msg_id, uint8_t *p_data);
void RE_CAN_WriteMsg(uint8_t *p_tx_msg);

#endif