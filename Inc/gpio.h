#ifndef __RE_GPIO_H
#define __RE_GPIO_H

#include "main.h"

#define GEAR_DRV GPIO_PIN_0
#define GEAR_REV GPIO_PIN_1

void RE_GPIO_Init(void);
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
#endif