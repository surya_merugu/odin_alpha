#ifndef RE_TIMERS_H
#define RE_TIMERS_H

#include "main.h"

extern TIM_HandleTypeDef htim2, htim3;

void RE_IWDG_Init(void);
void RE_RTC_Init(void);
void RE_TIM2_Init(void);
void RE_TIM3_Init(void);
void RE_TIM5_Init(void);

#endif