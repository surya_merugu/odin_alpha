/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C"
{
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdbool.h"
#include "stdio.h"
#include "string.h"
#include "can.h"
#include "gpio.h"
#if 0
#include "sd_card.h"
#endif
#include "eeprom.h"
#include "adc_sensors.h"
#include "timers.h"
#include "nrf52.h"
#include "ec20.h"
#include "pwr_mgmt.h"
#include "dash_lcd.h"
#include "app_dash_lcd.h"
#include "task.h"
#include "uart_ring_buffer.h"
    /* USER CODE END Includes */

    /* Exported types ------------------------------------------------------------*/
    /* USER CODE BEGIN ET */
    typedef struct system_state
    {
        uint8_t speed, key, latch, t_fuse, motor_ctrl, v48_ctrl, v12_ctrl, packs_count, bkbat_health, tamper, swap_mode, g2g, range, efficiency, error_code, inc_odo;
        uint8_t lcd_animate, gear_mode, dock_status;
        uint16_t ODO_MT, energy_left, FW_Ver;
        uint32_t ODO_KM;

        uint8_t bat_temp;
        uint16_t bat_volt;
        uint8_t bat_soc;
        uint8_t bat_soc_graph;
        uint8_t bat_fault_graph;
    } sys_t;
    /* USER CODE END ET */

    /* Exported constants --------------------------------------------------------*/
    /* USER CODE BEGIN EC */

    /* USER CODE END EC */

    /* Exported macro ------------------------------------------------------------*/
    /* USER CODE BEGIN EM */

    /* USER CODE END EM */

    /* Exported functions prototypes ---------------------------------------------*/
    void RE_Error_Handler(const char *pfile, uint16_t line);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LCD_CLK_Pin GPIO_PIN_13
#define LCD_CLK_GPIO_Port GPIOC
#define LCD_DI_Pin GPIO_PIN_14
#define LCD_DI_GPIO_Port GPIOC
#define GEAR_N_Pin GPIO_PIN_0
#define GEAR_N_GPIO_Port GPIOC
#define GEAR_N_EXTI_IRQn EXTI0_IRQn
#define GEAR_R_Pin GPIO_PIN_1
#define GEAR_R_GPIO_Port GPIOC
#define GEAR_R_EXTI_IRQn EXTI1_IRQn
#define CAN1_ERR_Pin GPIO_PIN_2
#define CAN1_ERR_GPIO_Port GPIOC
#define CAN1_ERR_EXTI_IRQn EXTI2_IRQn
#define CAN1_STB_Pin GPIO_PIN_3
#define CAN1_STB_GPIO_Port GPIOC
#define ADC_TEMP1_Pin GPIO_PIN_1
#define ADC_TEMP1_GPIO_Port GPIOA
#define LCD_CTRL_Pin GPIO_PIN_14
#define LCD_CTRL_GPIO_Port GPIOB
#define EC20_RESET_Pin GPIO_PIN_7
#define EC20_RESET_GPIO_Port GPIOC
#define LED_Pin GPIO_PIN_9
#define LED_GPIO_Port GPIOC
#define EC20_WAKE_Pin GPIO_PIN_8
#define EC20_WAKE_GPIO_Port GPIOA
#define EC20_PWR_Pin GPIO_PIN_15
#define EC20_PWR_GPIO_Port GPIOA
#define EC20_IO_Pin GPIO_PIN_4
#define EC20_IO_GPIO_Port GPIOC
#define CAN1_EN_Pin GPIO_PIN_4
#define CAN1_EN_GPIO_Port GPIOB
#define BUZZ_Pin GPIO_PIN_5
#define BUZZ_GPIO_Port GPIOB
#define LCD_CS1_Pin GPIO_PIN_8
#define LCD_CS1_GPIO_Port GPIOB
#define LCD_CS2_Pin GPIO_PIN_9
#define LCD_CS2_GPIO_Port GPIOB

    void AppendLog(char *pFileName, char *pData);
    void OverWriteLog(char *pFileName, char *pData);
    uint32_t ReadLogFile(char *pFileName, uint8_t len);

    //void MX_GPIO_Init(void);
    void MX_DMA_Init(void);
    //void MX_CAN1_Init(void);
    //void MX_SDIO_SD_Init(void);
    //void MX_USART1_UART_Init(void);
    //void MX_USART2_UART_Init(void);
    //void MX_I2C1_Init(void);
    //void MX_ADC1_Init(void);
    //void MX_IWDG_Init(void);
    //void MX_RTC_Init(void);
    //void MX_TIM2_Init(void);
    //void MX_TIM3_Init(void);
    //void MX_TIM5_Init(void);
    /* USER CODE BEGIN Private defines */

    /* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
