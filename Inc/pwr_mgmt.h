#ifndef __RE_PWR_MGMT_H
#define __RE_PWR_MGMT_H

#include "main.h"

///**
//     * @brief  Application Status structures definition
//     */
//typedef enum
//{
//    RE_OK = 0x00U,
//    RE_ERROR = 0x01U,
//    RE_BUSY = 0x02U,
//    RE_TIMEOUT = 0x03U
//} RE_StatusTypeDef;

void RE_PwrMgmtRun();
#endif