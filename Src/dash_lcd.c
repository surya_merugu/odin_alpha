/* Includes */
#include "dash_lcd.h"
#include "stdbool.h"

GPIO_InitTypeDef cs1_pin, cs2_pin, clk_pin, data_pin, lcd_ctrl;

static RE_StatusTypeDef RE_lcd_write_bit(bool n);
static RE_StatusTypeDef RE_lcd_write_cmdcode(uint8_t cmdcode);

/**
  * @Brief RE_LCD_Init
  * This function iniialises the GPIO pins used by Dashboard LCD
  * @Param None
  * @Retval Exit status
  */
void RE_LCD_Init(void)
{
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /*
    * PB8  : cs1_pin
    * PB9  : cs2_pin
    * PC13 : clk_pin
    * PC14 : data_pin
    * PB14 : Lcd_Ctrl
    */
    cs1_pin.Pin = GPIO_PIN_8;
    cs1_pin.Mode = GPIO_MODE_OUTPUT_PP;
    cs1_pin.Pull = GPIO_PULLDOWN;
    cs1_pin.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOB, &cs1_pin);

    cs2_pin.Pin = GPIO_PIN_9;
    cs2_pin.Mode = GPIO_MODE_OUTPUT_PP;
    cs2_pin.Pull = GPIO_PULLDOWN;
    cs2_pin.Speed = GPIO_SPEED_LOW;
    HAL_GPIO_Init(GPIOB, &cs2_pin);

    clk_pin.Pin = GPIO_PIN_13;
    clk_pin.Mode = GPIO_MODE_OUTPUT_PP;
    clk_pin.Pull = GPIO_PULLDOWN;
    clk_pin.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &clk_pin);

    data_pin.Pin = GPIO_PIN_14;
    data_pin.Mode = GPIO_MODE_OUTPUT_PP;
    data_pin.Pull = GPIO_PULLDOWN;
    data_pin.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GPIOC, &data_pin);

    lcd_ctrl.Pin = GPIO_PIN_14;
    lcd_ctrl.Mode = GPIO_MODE_OUTPUT_PP;
    lcd_ctrl.Pull = GPIO_NOPULL;
    lcd_ctrl.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &lcd_ctrl);
}

/**
  * @Brief RE_lcd_write_bit
  * This function bit bang the the lcd data to be sent onto GPIO
  * @Param n bit: [1] or [0]
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_lcd_write_bit(bool n)
{
    HAL_GPIO_WritePin(GPIOC, data_pin.Pin, (GPIO_PinState)n);
    __NOP();
    __NOP();
    HAL_GPIO_WritePin(GPIOC, clk_pin.Pin, GPIO_PIN_SET); /* Write bit '1' to GPIO */
    __NOP();
    __NOP();
    HAL_GPIO_WritePin(GPIOC, clk_pin.Pin, GPIO_PIN_RESET); /* Write bit '0' to GPIO */
    __NOP();
    __NOP();
    HAL_GPIO_WritePin(GPIOC, clk_pin.Pin, GPIO_PIN_SET); /* Write bit '1' to GPIO */
    return RE_OK;
}

/**
  * @Brief RE_lcd_write_data
  * Send 6-bit address + 8-bit data to <address>
  * @Param address Lcd memory address
  * @Param data Data to be written onto LCD memory
  * @Param cs_pin Chip select pin for lcd memory
  * @Retval Exit status
  */
RE_StatusTypeDef RE_lcd_write_data(uint8_t address, uint64_t data, uint16_t cs_pin)
{
    uint8_t i = 0;
    HAL_GPIO_WritePin(GPIOB, cs_pin, GPIO_PIN_SET);   /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs_pin, GPIO_PIN_RESET); /* Write bit '1' to GPIO */
    RE_lcd_write_bit(1);
    RE_lcd_write_bit(0);
    RE_lcd_write_bit(1);
    address <<= 2;
    for (i = 0; i < 6; i++)
    {
        if ((address & 0x80) == 0x80)
            RE_lcd_write_bit(1);
        else
            RE_lcd_write_bit(0);
        address <<= 1;
    }
    for (i = 0; i < 64; i++)
    {
        if ((data & 0x01) == 0x01)
            RE_lcd_write_bit(1);
        else
            RE_lcd_write_bit(0);
        data >>= 1;
    }
    HAL_GPIO_WritePin(GPIOC, data_pin.Pin, GPIO_PIN_SET); /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs_pin, GPIO_PIN_SET);       /* Write bit '1' to GPIO */
    return RE_OK;
}

/**
  * @Brief RE_lcd_write_data
  * Send control word
  * @Param cmdcode Read, Write, Reset command code
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_lcd_write_cmdcode(uint8_t cmdcode)
{
    uint8_t i = 0;
    HAL_GPIO_WritePin(GPIOB, cs1_pin.Pin, GPIO_PIN_SET);   /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs2_pin.Pin, GPIO_PIN_SET);   /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs1_pin.Pin, GPIO_PIN_RESET); /* Write bit '0' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs2_pin.Pin, GPIO_PIN_RESET); /* Write bit '0' to GPIO */
    RE_lcd_write_bit(1);
    RE_lcd_write_bit(0);
    RE_lcd_write_bit(0);
    for (i = 0; i < 8; i++)
    {
        if ((cmdcode & 0x80) == 0x80)
            RE_lcd_write_bit(1);
        else
            RE_lcd_write_bit(0);
        cmdcode <<= 1;
    }
    RE_lcd_write_bit(0);
    HAL_GPIO_WritePin(GPIOC, data_pin.Pin, GPIO_PIN_SET); /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs1_pin.Pin, GPIO_PIN_SET);  /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs2_pin.Pin, GPIO_PIN_SET);  /* Write bit '1' to GPIO */
    return RE_OK;
}

/**
  * @Brief RE_lcd_reset
  * Reset and initialize LCD
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_lcd_reset(void)
{
    RE_lcd_write_cmdcode(CMDOFF);    //Power down
    RE_lcd_write_cmdcode(CMDLCDOFF); //Close off
    RE_lcd_write_cmdcode(CMDON);     //Power on
    RE_lcd_write_cmdcode(CMDLCDON);  //Display
    RE_lcd_write_cmdcode(CMDB3C4);   //Mode setting
    return RE_OK;
}

/**
  * @Brief RE_lcd_reset
  * Reset and initialize LCD
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_lcd_power_off(void)
{
    HAL_GPIO_WritePin(GPIOC, data_pin.Pin, GPIO_PIN_RESET); /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs1_pin.Pin, GPIO_PIN_RESET);  /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOB, cs2_pin.Pin, GPIO_PIN_RESET);  /* Write bit '1' to GPIO */
    HAL_GPIO_WritePin(GPIOC, clk_pin.Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
    return RE_OK;
}

/*********************** (C) COPYRIGHT RACEnergy **********END OF FILE********/