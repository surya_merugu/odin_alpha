/**
  ******************************************************************************
  * @file           : can.c
  * @brief          : CAN peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "can.h"

extern sys_t sys_info;
extern Queue_t *p_Event_Queue;
extern Queue_t *p_Task_Queue;

CAN_HandleTypeDef hcan1;
CAN_TxHeaderTypeDef TxHeader;
CAN_RxHeaderTypeDef RxHeader;
Queue_t *p_CanRxQueue;
Queue_t *p_CanTxQueue;

static void RE_CAN1_Filter_Config(void);
static void RE_CAN1_Start_Interrupt(void);

uint8_t temperature[4], soc[4];
uint16_t volt[4];

/**
  * @brief CAN1 Initialization Function
  * @param None
  * @retval None
  */
void RE_CAN1_Init(void)
{

    /* USER CODE BEGIN CAN1_Init 0 */
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, CAN1_EN_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, CAN1_STB_Pin, GPIO_PIN_SET);
    /*Configure GPIO pins :  CAN1_EN_Pin */
    GPIO_InitStruct.Pin = CAN1_EN_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins :  CAN1_STB_Pin */
    GPIO_InitStruct.Pin = CAN1_STB_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /*Configure GPIO pin : CAN1_ERR_Pin */
    GPIO_InitStruct.Pin = CAN1_ERR_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* USER CODE END CAN1_Init 0 */

    /* USER CODE BEGIN CAN1_Init 1 */

    /* USER CODE END CAN1_Init 1 */
    hcan1.Instance = CAN1;
    hcan1.Init.Prescaler = 5;
    hcan1.Init.Mode = CAN_MODE_NORMAL;
    hcan1.Init.SyncJumpWidth = CAN_SJW_1TQ;
    hcan1.Init.TimeSeg1 = CAN_BS1_15TQ;
    hcan1.Init.TimeSeg2 = CAN_BS2_2TQ;
    hcan1.Init.TimeTriggeredMode = DISABLE;
    hcan1.Init.AutoBusOff = ENABLE;
    hcan1.Init.AutoWakeUp = ENABLE;
    hcan1.Init.AutoRetransmission = ENABLE;
    hcan1.Init.ReceiveFifoLocked = DISABLE;
    hcan1.Init.TransmitFifoPriority = DISABLE;
    if (HAL_CAN_Init(&hcan1) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /* USER CODE BEGIN CAN1_Init 2 */

    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);

    /* CAN1_TX_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(CAN1_TX_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);
    /* CAN1_RX0_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(CAN1_RX0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
    /* CAN1_RX1_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(CAN1_RX1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);
    /* CAN1_SCE_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(CAN1_SCE_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(CAN1_SCE_IRQn);


    p_CanTxQueue = RE_CreateQueue(TxQueueItems, TxQueueItemLen); /* A queue of {TxQueueItems} items of each {TxQueueItemLen} bytes in length */
    p_CanRxQueue = RE_CreateQueue(RxQueueItems, RxQueueItemLen); /* A queue of {RxQueueItems} items of each {RxQueueItemLen} bytes in length */
    
    RE_CAN1_Filter_Config();
    RE_CAN1_Start_Interrupt();
    /* USER CODE END CAN1_Init 2 */
}

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan1)
{
    uint8_t queue_item[RxQueueItemLen];
    if (HAL_CAN_GetRxMessage(hcan1, CAN_RX_FIFO0, &RxHeader, &queue_item[5]) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    queue_item[0] = (uint8_t)(RxHeader.ExtId >> 24);
    queue_item[1] = (uint8_t)(RxHeader.ExtId >> 16);
    queue_item[2] = (uint8_t)(RxHeader.ExtId >> 8);
    queue_item[3] = (uint8_t)(RxHeader.ExtId);
    queue_item[4] = RxHeader.DLC;
    RE_EnQueue(p_CanRxQueue, queue_item, RxQueueItemLen);
}

#if 0
void RE_CAN_ReadMsg(uint8_t *p_CanRxMsg)
{
    /* p_CanRxMsg[0:3] = { 29-bit CAN ID } */
    /* p_CanRxMsg[4] = DLC */
    uint8_t node_id = p_CanRxMsg[2], mode = 0, task_id = p_CanRxMsg[1], dock_id = (p_CanRxMsg[1] & 0x0F);
    switch (node_id)
    {
    case 0xA1:
        /* Msg from Battery */
        RE_CAN_Read_Bat_Msg(dock_id, p_CanRxMsg[0], &p_CanRxMsg[4]); /* p_CanRxMsg[4] = DLC */
        break;
    case 0xA2:                                                /* From FRIGG */
        RE_CAN_Read_Frigg_Msg(p_CanRxMsg[0], &p_CanRxMsg[4]); /* p_CanRxMsg[4] = DLC */
        break;
    case 0xA3: /* Msg to/from FRIGG EEPROM */
        switch (task_id)
        {
        case 0x10: /* Write to EEPROM [0x00A11001] */
        case 0x11: /* Read from EEPROM [0x00A11001] */
            mode = ((p_CanRxMsg[1] & 0x0F) == 0) ? EEPROM_WRITE : EEPROM_READ;
            /* TODO: */
            //        RE_CAN_Write_EEPROM(&p_data[2]);
            /* code */
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}
#endif
void RE_CAN_WriteMsg(uint8_t *p_tx_msg)
{
    bool MailBoxEmpty = false;
    uint32_t tx_mail_box, tms = 0;
    uint32_t can_id = (uint32_t)(p_tx_msg[0] << 24 | p_tx_msg[1] << 16 | p_tx_msg[2] << 8 | p_tx_msg[3]);
    TxHeader.DLC = p_tx_msg[4]; /* DLC */
    TxHeader.ExtId = can_id;
    TxHeader.IDE = CAN_ID_EXT;
    TxHeader.RTR = CAN_RTR_DATA;
    if (TxHeader.DLC < 9)
    {
        tms = HAL_GetTick();
        while((HAL_GetTick() - tms) < 30000)
        {
          if(HAL_CAN_GetTxMailboxesFreeLevel(&hcan1) != 0)
          {
            MailBoxEmpty = true;
            break;
          }
        }
        if(MailBoxEmpty == true)
        {
          if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, &p_tx_msg[5], &tx_mail_box) != HAL_OK)
          {
              RE_Error_Handler(__FILE__, __LINE__);
          }
        }
    }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan)
{
    /* TODO: Rec error event */
    // HAL_NVIC_SystemReset();
}

void RE_CAN_Format_TxMsg(uint8_t dest_node_id, uint8_t dock_id, uint8_t msg_id, uint8_t *p_data)
{
    uint8_t msg_item[TxQueueItemLen] = {0};
    msg_item[0] = Tx_NODE_ODIN >> 8;                      /* CAN_ID[3] */
    msg_item[1] = (uint8_t)(Tx_NODE_ODIN | dest_node_id); /* CAN_ID[2] */
    msg_item[2] = dock_id;
    msg_item[3] = msg_id;
    msg_item[4] = p_data[0]; /* DLC */
    for (uint8_t i = 0; i < msg_item[4]; i++)
    {
        msg_item[5 + i] = p_data[1 + i];
    }
    RE_EnQueue(p_CanTxQueue, msg_item, TxQueueItemLen);
}

void RE_update_Bat_Data(void)
{
    uint8_t min_soc = soc[0], max_temp = temperature[0];
    uint16_t min_volt = volt[0];
    
    /**@Cause: if in case any pack CAN is not communicating the array elements of volt and soc are filled with zeros, which 
    intern shows least voltage and SoC on the screen as zeros. so to mitigate that the array elements which are filled with 
    are incremented to the highest value then the ideal values of SoC and voltage to show least voltage*/
    /**@Note: The below fix in conditional statement is temporary.*/
    if(soc[0] == 0 & soc[1] == 0 & soc[2] == 0 & soc[3] == 0)
    {
        sys_info.bat_soc = 0;
        sys_info.bat_temp = 0;
        sys_info.bat_volt = 0;
        return;
    }
    else
    {
        __NOP();
    }
#if 0
    if(volt[0] == 0 & volt[1] == 0 & volt[2] == 0 & volt[3] == 0)
    {
        min_volt = 0;
        max_temp = 0;
        return;
    }
    else
    {
        __NOP();
    }
    if(temperature[0] == 0 & temperature[1] == 0 & temperature[2] == 0 & temperature[3] == 0)
    {
        max_temp = 0;
        return;
    }
    else
    {
        __NOP();
    }
#endif
    for(uint8_t i = 0; i < 4; i++)
    {
        if(volt[i] == 0)
        {
            volt[i] = 700;
        }
        if(soc[i] == 0)
        {
            soc[i] = 150;
        }
    }
    for (uint8_t i = 0; i < 4; i++)
    {
        if (soc[i] < min_soc)
        {
            min_soc = soc[i];
        }
        if (temperature[i] > max_temp)
        {
            max_temp = temperature[i];
        }
        if (volt[i] < min_volt)
        {
            min_volt = volt[i];
        }
#if 0
        if (min_volt == 0)
        {
            /* Error code */
            LcdDisplay.err_code = 2;
        }
        else
        {
            LcdDisplay.err_code = 0;
        }
#endif
    }
    sys_info.bat_soc = min_soc;
    sys_info.bat_temp = max_temp;
    sys_info.bat_volt = min_volt;
}

void RE_CAN_Read_Bat_Msg(uint8_t dock_id, uint8_t msg_id, uint8_t *p_data)
{
    /* p_data[0] = DLC */
    /* p_data[1:8] = CAN DATA */
    switch (msg_id)
    {
    case 0x01:
        __NOP();
        break;
    case 0x09: /* Gauge short packet  */
    if(dock_id == 1 | dock_id == 2 | dock_id == 3 | dock_id == 4) 
    {
        sys_info.dock_status |= (1 << (dock_id - 1));
        soc[dock_id - 1] = p_data[1];
        volt[dock_id - 1] = (uint16_t)(p_data[2] + 350);
        temperature[dock_id - 1] = p_data[5];
    }
    else
    {
        return;
    }
    break;
    default:
        break;
    }
}

void RE_CAN_Read_Frigg_Msg(uint8_t msg_id, uint8_t *p_data)
{
    /* p_data[0] = DLC */
    /* p_data[1:8] = CAN DATA */
    uint8_t io_event[2] = {0}, tx_msg = 0x00;
    switch (msg_id)
    {
    case 0x01:
        sys_info.speed = p_data[1]; /* Speed in KMPH */
#if 0
        sys_info.key = (p_data[2] & 0x01);               /* KEY Status => 1 = ON; 0 = OFF*/
#endif
        sys_info.latch = ((p_data[2] >> 1) & 0x01);      /* Latch Status => 1 = OPEN; 0 = CLOSE */
        sys_info.t_fuse = ((p_data[2] >> 2) & 0x01);     /* T_FUSE => 1 = CUTOFF; 0 = NO_CUTOFF */
        sys_info.motor_ctrl = ((p_data[2] >> 3) & 0x01); /* MOTOR_CTRL => 1 = OFF; 0 = ON*/
        sys_info.v48_ctrl = ((p_data[2] >> 4) & 0x01);   /* 48V_CTRL => 1 = OFF; 0 = ON */
        sys_info.v12_ctrl = ((p_data[2] >> 5) & 0x01);   /* 12V_CTRL => 1 = OFF; 0 = ON */
        sys_info.tamper = ((p_data[2] >> 6) & 0x01);     /* Tamper => 1 = ALERT; 0 = NO_Alert */
        sys_info.inc_odo = ((p_data[2] >> 7) & 0x01);    /* INC_ODO => 1 = ODO++; 0 = No increment */
        sys_info.packs_count = (p_data[3] & 0x0F);       /* LSB = Pack count */
        sys_info.bkbat_health = (p_data[3] >> 4);        /* MSB = BK bat health */
        sys_info.range = p_data[4];
        sys_info.efficiency = p_data[5];
        sys_info.error_code = p_data[6];
        sys_info.ODO_MT = (p_data[7] << 8 | p_data[8]);
        if (sys_info.inc_odo == 0x01)
        {
            sys_info.ODO_KM++;
        }
        break;
    case 0x02: /* Msg on key on. [ODO_KM] */
        sys_info.ODO_KM = (p_data[1] << 24 | p_data[2] << 16 | p_data[3] << 8 | p_data[4]);
        sys_info.ODO_MT = p_data[5] << 8 | p_data[6];
        sys_info.key = p_data[7];
        if (sys_info.key == 0)
        {
            io_event[0] = 0x03; /*  Msg ID */
            io_event[1] = 0x00;
            /* KEY_OFF EVENT */
            RE_EnQueue(p_Event_Queue, io_event, EVT_QUEUE_ITEM_LEN);
            RE_CAN_Format_TxMsg(RX_NODE_BATTERY, 0x00, 0x01, &tx_msg);      //requesting phy id's
            
        }
        else
        {
            /* Enqueue LCD animate task */
            io_event[0] = 0x02; /*  Msg ID */
            io_event[1] = 0x00; /* Value */
            /* KEY_ON EVENT */
            RE_EnQueue(p_Event_Queue, io_event, EVT_QUEUE_ITEM_LEN);
            RE_CAN_Format_TxMsg(RX_NODE_BATTERY, 0x00, 0x01, &tx_msg);      //requesting phy id's
        }
        break;
    case 0x03:      /**Latch interrupt*/
        sys_info.latch = p_data[1];
        RE_CAN_Format_TxMsg(RX_NODE_BATTERY, 0x00, 0x01, &tx_msg);         //requesting phy id's
        break;
    default:
        break;
    }
}

/**
  * @brief RE_CAN1_Filter_Config
  *  This function configures CAN1 filter banks
  * @param None
  * @retval None
  */
static void RE_CAN1_Filter_Config(void)
{
    CAN_FilterTypeDef CAN1_Filter_t;

    /*Odin CAN Filter*/

    /* 0x00A3xxxx - Battery to Odin */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 0; /* FilterBank:0 */
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0518;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }

    /* 0x00B3xxxx - FRIGG to Odin */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 1; /* FilterBank:1 */
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0598;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
#if 0    
    /* 0x00A3xxxx - Battery to Odin */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 0; /* FilterBank:0 */
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0518;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFF8;
    CAN1_Filter_t.FilterMaskIdLow = 0x0004;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    
    /*  0x00B300xx :- Frigg to odin */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 1; /* FilterBank:1 */
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0598;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFFF;
    CAN1_Filter_t.FilterMaskIdLow = 0xF804;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }

    /* 0x00C100xx- To Odin EEPROM */
    CAN1_Filter_t.FilterActivation = ENABLE;
    CAN1_Filter_t.FilterBank = 2;
    CAN1_Filter_t.FilterFIFOAssignment = CAN_RX_FIFO0;
    CAN1_Filter_t.FilterIdHigh = 0x0608;
    CAN1_Filter_t.FilterIdLow = 0x0004;
    CAN1_Filter_t.FilterMaskIdHigh = 0xFFFF;
    CAN1_Filter_t.FilterMaskIdLow = 0xF804;
    CAN1_Filter_t.FilterMode = CAN_FILTERMODE_IDMASK;
    CAN1_Filter_t.FilterScale = CAN_FILTERSCALE_32BIT;
    if (HAL_CAN_ConfigFilter(&hcan1, &CAN1_Filter_t) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
#endif
}

/**
  * @brief RE_CAN1_Start_Interrupt
  *  This function switches CAN1 from initialisation mode to start mode
  * @param None
  * @retval None
  */
static void RE_CAN1_Start_Interrupt(void)
{
    if (HAL_CAN_ActivateNotification(&hcan1, CAN_IT_TX_MAILBOX_EMPTY | CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_LAST_ERROR_CODE |
                                                 CAN_IT_RX_FIFO1_MSG_PENDING | CAN_IT_BUSOFF | CAN_IT_ERROR | CAN_IT_ERROR_PASSIVE | CAN_IT_ERROR_WARNING) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /* CAN enter normal operation mode*/
    if (HAL_CAN_Start(&hcan1) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
}