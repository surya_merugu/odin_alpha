
#include "uart_ring_buffer.h"

volatile bool uart1RxCmplt = false, uart2RxCmplt = false;
extern UART_HandleTypeDef huart2, huart1;
extern Queue_t *p_UartRxQueue;
extern Queue_t *p_UartTxQueue;
uint8_t ble_rcvd_char_count = 0;

ring_buffer_uart1 rx_buffer_uart1 = {{0}, 0, 0};
ring_buffer_uart1 tx_buffer_uart1 = {{0}, 0, 0};
ring_buffer_uart1 *_rx_buffer_uart1;
ring_buffer_uart1 *_tx_buffer_uart1;
ring_buffer_uart2 rx_buffer_uart2 = {{0}, 0, 0};
ring_buffer_uart2 tx_buffer_uart2 = {{0}, 0, 0};
ring_buffer_uart2 *_rx_buffer_uart2;
ring_buffer_uart2 *_tx_buffer_uart2;

/**
  * @Brief Ringbuf_Init
  * This function initialises the ring buffer and also enables interrupts
  * @Param None
  * @Retval None
  */
void Ringbuf_Init(void)
{
    _rx_buffer_uart1 = &rx_buffer_uart1;
    _tx_buffer_uart1 = &tx_buffer_uart1;
    _rx_buffer_uart2 = &rx_buffer_uart2;
    _tx_buffer_uart2 = &tx_buffer_uart2;

    /* Enable the UART Error Interrupt: (Frame error, noise error, overrun error) */
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_ERR);
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_ERR);
    /* Enable the UART Data Register not empty Interrupt */
    __HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE);
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
}

/**
  * @Brief Ringbuf_UART1_Free
  * This function frees the ring buffer of UART2
  * @Param None
  * @Retval None
  */
void Ringbuf_UART1_Free(void)
{
    memset(_rx_buffer_uart1->buffer_uart1, 0x00, UART1_BUFFER_SIZE);
    _rx_buffer_uart1->head_uart1 = NULL;
    _rx_buffer_uart1->tail_uart1 = NULL;
    memset(_tx_buffer_uart1->buffer_uart1, 0x00, UART1_BUFFER_SIZE);
    _tx_buffer_uart1->head_uart1 = NULL;
    _tx_buffer_uart1->tail_uart1 = NULL;
}

/**
  * @Brief Ringbuf_UART2_Free
  * This function frees the ring buffer of UART2
  * @Param None
  * @Retval None
  */
void Ringbuf_UART2_Free(void)
{
    memset(_rx_buffer_uart2->buffer_uart2, 0x00, UART2_BUFFER_SIZE);
    _rx_buffer_uart2->head_uart2 = NULL;
    _rx_buffer_uart2->tail_uart2 = NULL;
    memset(_tx_buffer_uart2->buffer_uart2, 0x00, UART2_BUFFER_SIZE);
    _tx_buffer_uart2->head_uart2 = NULL;
    _tx_buffer_uart2->tail_uart2 = NULL;
}

/**
  * @Brief Store_UART1_Char
  * This function stores the received charcter from UART2 into UART2 ring buffer
  * @Param char 
  * @Param pointer to ting buffer
  * @Retval None
  */
void Store_UART1_Char(unsigned char c, ring_buffer_uart1 *buffer)
{
    uint32_t i = (uint32_t)(buffer->head_uart1 + 1) % UART1_BUFFER_SIZE;
    /** 
   * if we should be storing the received character into the location
   * just before the tail (meaning that the head would advance to the
   * current location of the tail), we're about to overflow the buffer
   * and so we don't write the character or advance the head.
   */
    if (i != buffer->tail_uart1)
    {
        buffer->buffer_uart1[buffer->head_uart1] = c;
        buffer->head_uart1 = i;
    }
}

/**
  * @Brief Store_UART2_Char
  * This function stores the received charcter from UART2 into UART2 ring buffer
  * @Param char 
  * @Param pointer to ting buffer
  * @Retval None
  */
void Store_UART2_Char(unsigned char c, ring_buffer_uart2 *buffer)
{
    uint32_t i = (uint32_t)(buffer->head_uart2 + 1) % UART2_BUFFER_SIZE;
    /** 
   * if we should be storing the received character into the location
   * just before the tail (meaning that the head would advance to the
   * current location of the tail), we're about to overflow the buffer
   * and so we don't write the character or advance the head.
   */
    if (i != buffer->tail_uart2)
    {
        buffer->buffer_uart2[buffer->head_uart2] = c;
        buffer->head_uart2 = i;
    }
}

#if 0
static int UART2_Read(void)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_uart2->head_uart2 == _rx_buffer_uart2->tail_uart2)
    {
        return -1;
    }
    else
    {
        unsigned char c = _rx_buffer_uart2->buffer_uart2[_rx_buffer_uart2->tail_uart2];
        _rx_buffer_uart2->tail_uart2 = (unsigned int)(_rx_buffer_uart2->tail_uart2 + 1) % UART_BUFFER_SIZE;
        return c;
    }
}


static int UART3_Read(void)
{
    // if the head isn't ahead of the tail, we don't have any characters
    if (_rx_buffer_uart3->head_uart3 == _rx_buffer_uart3->tail_uart3)
    {   
        return -1;
    }
    else
    {
        unsigned char c = _rx_buffer_uart3->buffer_uart3[_rx_buffer_uart3->tail_uart3];
        _rx_buffer_uart3->tail_uart3 = (unsigned int)(_rx_buffer_uart3->tail_uart3 + 1) % UART_BUFFER_SIZE;
        return c;
    }
}


//make this better
void Get_UART2_String(char *buffer)
{
    int index = 0;
    unsigned int start = _rx_buffer_uart2->tail_uart2;
    unsigned int end = (_rx_buffer_uart2->head_uart2);
    if ((_rx_buffer_uart2->tail_uart2 > _rx_buffer_uart2->head_uart2) && (_rx_buffer_uart2->buffer_uart2[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < UART_BUFFER_SIZE; i++)
        {
            buffer[index] = UART2_Read();
            index++;
        }
        for (unsigned int i = 0; i < end; i++)
        {
            buffer[index] = UART2_Read();
            index++;
        }
    }
    else if ((_rx_buffer_uart2->buffer_uart2[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < end; i++)
        {
                buffer[index] = UART2_Read();
                index++;
        }
    }
}


void Get_UART3_String(char *buffer)
{
    int index = 0;
    unsigned int start = _rx_buffer_uart3->tail_uart3;
    unsigned int end = (_rx_buffer_uart3->head_uart3);
    if ((_rx_buffer_uart3->tail_uart3 > _rx_buffer_uart3->head_uart3) && (_rx_buffer_uart3->buffer_uart3[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < UART_BUFFER_SIZE; i++)
        {
            buffer[index] = UART3_Read();
            index++;
        }
        for (unsigned int i = 0; i < end; i++)
        {
                buffer[index] = UART3_Read();
                index++;
        }
    }
    else if ((_rx_buffer_uart3->buffer_uart3[end - 1] == '\n'))
    {
        for (unsigned int i = start; i < end; i++)
        {
                buffer[index] = UART3_Read();
                index++;
        }
    }
}
#endif

/**
  * @Brief This function handles UART global interrupt.
  */
void UART_ISR(UART_HandleTypeDef *huart)
{
    if (huart->Instance == USART1)
    {
        uint32_t isrflags = READ_REG(huart->Instance->SR);
        uint32_t cr1its = READ_REG(huart->Instance->CR1);
        /* if DR is not empty and the Rx Int is enabled */
        if (((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
        {
            /******************
            * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
            *         error) and IDLE (Idle line detected) flags are cleared by software
            *         sequence: a read operation to USART_SR register followed by a read
            *         operation to USART_DR register.
            * @note   RXNE flag can be also cleared by a read to the USART_DR register.
            * @note   TC flag can be also cleared by software sequence: a read operation to
            *         USART_SR register followed by a write operation to USART_DR register.
            * @note   TXE flag is cleared only by a write to the USART_DR register.
            *********************/
            huart->Instance->SR;                   // Read status register
            unsigned char c = huart->Instance->DR; // Read data register
            Store_UART1_Char(c, _rx_buffer_uart1); // store data in buffer

            if ('@' == c)
            {
                //                memcpy(&ring_buffer[char_count], rx_buffer_uart1.buffer_uart1, 32);
                //                char_count = char_count + strlen((char *)rx_buffer_uart1.buffer_uart1);
                //                uart1RxCmplt = true;
                //                __NOP();
                RE_EnQueue(p_UartRxQueue, rx_buffer_uart1.buffer_uart1, UartRxQueueItemsLen);
                Ringbuf_UART1_Free();
                // Increment counter for every packet received.

                //1.  Push data into FIFO. FIFO_Len = 4 x rx_buff
                //2.  Clear rx_buffer
            }
            return;
        }
        /*If interrupt is caused due to Transmit Data Register Empty */
        if (((isrflags & USART_SR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
        {
            if (tx_buffer_uart1.head_uart1 == tx_buffer_uart1.tail_uart1)
            {
                // Buffer empty, so disable interrupts
                __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
            }
            else
            {
                // There is more data in the output buffer. Send the next byte
                unsigned char c = tx_buffer_uart1.buffer_uart1[tx_buffer_uart1.tail_uart1];
                tx_buffer_uart1.tail_uart1 = (tx_buffer_uart1.tail_uart1 + 1) % UART2_BUFFER_SIZE;
                /******************
                * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
                *         error) and IDLE (Idle line detected) flags are cleared by software
                *         sequence: a read operation to USART_SR register followed by a read
                *         operation to USART_DR register.
                * @note   RXNE flag can be also cleared by a read to the USART_DR register.
                * @note   TC flag can be also cleared by software sequence: a read operation to
                *         USART_SR register followed by a write operation to USART_DR register.
                * @note   TXE flag is cleared only by a write to the USART_DR register.
                *********************/
                huart->Instance->SR;
                huart->Instance->DR = c;
            }
            return;
        }
    }
    else if (huart->Instance == USART2)
    {
        uint32_t isrflags = READ_REG(huart->Instance->SR);
        uint32_t cr1its = READ_REG(huart->Instance->CR1);
        /* if DR is not empty and the Rx Int is enabled */
        if (((isrflags & USART_SR_RXNE) != RESET) && ((cr1its & USART_CR1_RXNEIE) != RESET))
        {
            /******************
            * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
            *         error) and IDLE (Idle line detected) flags are cleared by software
            *         sequence: a read operation to USART_SR register followed by a read
            *         operation to USART_DR register.
            * @note   RXNE flag can be also cleared by a read to the USART_DR register.
            * @note   TC flag can be also cleared by software sequence: a read operation to
            *         USART_SR register followed by a write operation to USART_DR register.
            * @note   TXE flag is cleared only by a write to the USART_DR register.
            *********************/
            huart->Instance->SR;                   // Read status register
            unsigned char c = huart->Instance->DR; // Read data register
            ble_rcvd_char_count++;
            Store_UART2_Char(c, _rx_buffer_uart2); // store data in buffer
                                                   /** if'c' == '\n'*/
            if (('#' == c) || (ble_rcvd_char_count >= 16))
            {
                ble_rcvd_char_count = 0;
                uart2RxCmplt = true;
                // Increment counter for every packet received.

                //1.  Push data into FIFO. FIFO_Len = 4 x rx_buff
                //2.  Clear rx_buffer
            }
            return;
        }
        /*If interrupt is caused due to Transmit Data Register Empty */
        if (((isrflags & USART_SR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET))
        {
            if (tx_buffer_uart2.head_uart2 == tx_buffer_uart2.tail_uart2)
            {
                // Buffer empty, so disable interrupts
                __HAL_UART_DISABLE_IT(huart, UART_IT_TXE);
            }
            else
            {
                // There is more data in the output buffer. Send the next byte
                unsigned char c = tx_buffer_uart2.buffer_uart2[tx_buffer_uart2.tail_uart2];
                tx_buffer_uart2.tail_uart2 = (tx_buffer_uart2.tail_uart2 + 1) % UART2_BUFFER_SIZE;
                /******************
                * @note   PE (Parity error), FE (Framing error), NE (Noise error), ORE (Overrun
                *         error) and IDLE (Idle line detected) flags are cleared by software
                *         sequence: a read operation to USART_SR register followed by a read
                *         operation to USART_DR register.
                * @note   RXNE flag can be also cleared by a read to the USART_DR register.
                * @note   TC flag can be also cleared by software sequence: a read operation to
                *         USART_SR register followed by a write operation to USART_DR register.
                * @note   TXE flag is cleared only by a write to the USART_DR register.
                *********************/
                huart->Instance->SR;
                huart->Instance->DR = c;
            }
            return;
        }
    }
}

/**
  * @Brief UART2_HandleData
  * This function handles the data in UART2 Ring buffer
  * @Param None
  * @Retval None
  */
void UART2_HandleData(void)
{
    uint8_t unlock_latch_cmd[16] = {0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D,
                                    0x1E, 0x1F, 0x20};
    uint8_t g2g_enable_cmd[16] = {0x21, 0x22, 0x22, 0x24, 0x25, 0x26, 0x27, 0x28, 0x29, 0x2A, 0x2B, 0x2C, 0x2D,
                                  0x2E, 0x2F, 0x30};
    uint8_t g2g_disable_cmd[16] = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3A, 0x3B, 0x3C, 0x3D,
                                   0x3E, 0x3F, 0x40};
    uint8_t mobile_ctrl[16];
    memcpy(mobile_ctrl, rx_buffer_uart2.buffer_uart2, 15);
    if (0 == memcmp(mobile_ctrl, unlock_latch_cmd, 15))
    {
        /** UNLOCK LATCH*/
        uint8_t buffer[2] = {0x01, 0x01}; /* DLC, 1: Unlock latch */
        /* Tx ODIN restarted */
        RE_CAN_Format_TxMsg(RX_NODE_FRIGG, 00, 0x02, buffer);
    }
    else if (0 == memcmp(mobile_ctrl, g2g_enable_cmd, 15))
    {
        uint8_t buffer[2] = {0x01, 0x00}; /* DLc, 1: G2G Enable*/
        RE_CAN_Format_TxMsg(RX_NODE_FRIGG, 00, 0x04, buffer);
    }
    else if (0 == memcmp(mobile_ctrl, g2g_disable_cmd, 15))
    {
        uint8_t buffer[2] = {0x01, 0x01}; /* DLc, 1: G2G Disable/swap mode enabled*/
        RE_CAN_Format_TxMsg(RX_NODE_FRIGG, 00, 0x04, buffer);
    }
    else
    {
        __NOP();
    }
    Ringbuf_UART2_Free();
}
/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/