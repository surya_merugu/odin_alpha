/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#if 0
#include "fatfs.h"
#endif

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DMA_HandleTypeDef hdma_adc1;

DMA_HandleTypeDef hdma_i2c1_rx;
DMA_HandleTypeDef hdma_i2c1_tx;

SD_HandleTypeDef hsd;
DMA_HandleTypeDef hdma_sdio_rx;
DMA_HandleTypeDef hdma_sdio_tx;

DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */
#if 0
extern char SDPath[4]; /* SD logical drive path */
extern FATFS SDFatFS;  /* File system object for SD logical drive */
extern FIL SDFile;     /* File object for SD */
FATFS *pfs;
FRESULT fres;
DWORD fre_clust;
uint32_t totalSpace, freeSpace;
uint32_t byteswritten, bytesread; /* File write/read counts */
uint8_t rtext[100];
char SD_Buffer[600];
#endif
sys_t sys_info;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
#if 0
static void clearbuf(void);
static void sd_mount(void);
#endif
static void RE_IdleStateHandle(void);

/* USER CODE END PFP */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* Configure the system clock */
    SystemClock_Config();

    RE_Create_EVT_Queue();
    RE_Create_TASK_Queue();

    /* Initialize all configured peripherals */
    RE_GPIO_Init();
    RE_LCD_Init();
    MX_DMA_Init();
    RE_CAN1_Init();
    RE_I2C1_Init();
    RE_USART1_UART_Init(); /* EC_20 */
    RE_USART2_UART_Init(); /* BLE */
    RE_TIM2_Init();
    RE_TIM3_Init();
    RE_Load_SystemStatus();
    if (HAL_TIM_Base_Start_IT(&htim2) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    if (HAL_TIM_Base_Start_IT(&htim3) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
//    HAL_GPIO_WritePin(GPIOC, EC20_IO_Pin, GPIO_PIN_SET);
#if 0
    uint8_t ResetCounter = 0x00;
    RE_EEPROM_Write(&ResetCounter, EC20_RESET_CNT_LEN, EC20_RESET_CNT_ADDR);
    RE_EEPROM_Read(&ResetCounter, EC20_RESET_CNT_LEN, EC20_RESET_CNT_ADDR);
    MX_SDIO_SD_Init();
    RE_ADC1_Init();
    RE_IWDG_Init();
    RE_RTC_Init();
    RE_TIM5_Init();
    MX_FATFS_Init();
    sd_mount();
#endif
    /* Infinite loop */
//    HAL_GPIO_WritePin(EC20_IO_GPIO_Port, EC20_IO_Pin, GPIO_PIN_SET);
    while (1)
    {
        RE_IdleStateHandle();
    }
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};
    /** Configure the main internal regulator output voltage
    */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
    /** Initializes the RCC Oscillators according to the specified parameters
    * in the RCC_OscInitTypeDef structure.
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
    RCC_OscInitStruct.LSIState = RCC_LSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 4;
    RCC_OscInitStruct.PLL.PLLN = 180;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 8;
    RCC_OscInitStruct.PLL.PLLR = 2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /** Activate the Over-Drive mode
    */
    if (HAL_PWREx_EnableOverDrive() != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /** Initializes the CPU, AHB and APB buses clocks
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_SDIO | RCC_PERIPHCLK_CLK48;
    PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
    PeriphClkInitStruct.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLQ;
    PeriphClkInitStruct.SdioClockSelection = RCC_SDIOCLKSOURCE_CLK48;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
}

#if 0
/**
  * @brief SDIO Initialization Function
  * @param None
  * @retval None
  */
void MX_SDIO_SD_Init(void)
{

    /* USER CODE BEGIN SDIO_Init 0 */

    /* USER CODE END SDIO_Init 0 */

    /* USER CODE BEGIN SDIO_Init 1 */

    /* USER CODE END SDIO_Init 1 */
    hsd.Instance = SDIO;
    hsd.Init.ClockEdge = SDIO_CLOCK_EDGE_RISING;
    hsd.Init.ClockBypass = SDIO_CLOCK_BYPASS_DISABLE;
    hsd.Init.ClockPowerSave = SDIO_CLOCK_POWER_SAVE_DISABLE;
    hsd.Init.BusWide = SDIO_BUS_WIDE_1B;
    hsd.Init.HardwareFlowControl = SDIO_HARDWARE_FLOW_CONTROL_DISABLE;
    hsd.Init.ClockDiv = 0;
    /* USER CODE BEGIN SDIO_Init 2 */

    /* USER CODE END SDIO_Init 2 */
}

#endif
/**
  * Enable DMA controller clock
  */
void MX_DMA_Init(void)
{

    /* DMA controller clock enable */
    __HAL_RCC_DMA2_CLK_ENABLE();
    __HAL_RCC_DMA1_CLK_ENABLE();

    /* DMA interrupt init */
    /* DMA1_Stream0_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Stream0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream0_IRQn);
    /* DMA1_Stream5_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
    /* DMA1_Stream6_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
    /* DMA1_Stream7_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA1_Stream7_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA1_Stream7_IRQn);
    /* DMA2_Stream0_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
    /* DMA2_Stream2_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
    /* DMA2_Stream3_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Stream3_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream3_IRQn);
    /* DMA2_Stream6_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Stream6_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream6_IRQn);
    /* DMA2_Stream7_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);
}

/* USER CODE BEGIN 4 */
#if 0
static void clearbuf(void)
{
    for (int i = 0; i < 600; i++)
    {
        SD_Buffer[i] = '\0';
    }
}

/**
  * @Mount SD Card
  */
static void sd_mount(void)
{
    if (f_mount(&SDFatFS, (TCHAR const *)SDPath, 0) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /************ Card capacity details ************/
    /* Check free space */
    if (f_getfree("", &fre_clust, &pfs) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    totalSpace = (uint32_t)((pfs->n_fatent - 2) * pfs->csize * 0.5);
    sprintf(SD_Buffer, "SD CARD Total Size: \t%lu\n", totalSpace);
    clearbuf();
    freeSpace = (uint32_t)(fre_clust * pfs->csize * 0.5);
    sprintf(SD_Buffer, "SD Card Free Space: \t%1u\n", freeSpace);
    clearbuf();
    if (f_open(&SDFile, "ODO.txt", FA_OPEN_ALWAYS | FA_WRITE) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
}

/** Log @pData into @pFile_name */
void AppendLog(char *pFileName, char *pData)
{
    if (f_open(&SDFile, pFileName, FA_OPEN_APPEND | FA_READ | FA_WRITE) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    f_puts(pData, &SDFile);
    if (f_close(&SDFile) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    clearbuf();
}

void OverWriteLog(char *pFileName, char *pData)
{
    if (f_open(&SDFile, pFileName, FA_OPEN_ALWAYS | FA_CREATE_ALWAYS | FA_WRITE) != FR_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /* Write data to text file */
    fres = f_write(&SDFile, pData, strlen((char *)pData), (void *)&byteswritten);
    if ((byteswritten == 0) || (fres != FR_OK))
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    f_close(&SDFile);
}

uint32_t ReadLogFile(char *pFileName, uint8_t len)
{
    int Data = 0;
    /* Open file to read */
    fres = f_open(&SDFile, pFileName, FA_READ);
    memset(rtext, 0, sizeof(rtext));
    fres = f_read(&SDFile, rtext, sizeof(rtext), (UINT *)&bytesread);
    if ((bytesread == 0) || (fres != FR_OK))
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    else
    {
        Data = atoi((const char *)rtext);
    }
    f_close(&SDFile);
    return Data;
}
#endif

static void RE_IdleStateHandle(void)
{
    if (RE_Task_Pending() == false)
    {
        RE_PwrMgmtRun();
    }
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void RE_Error_Handler(const char *pfile, uint16_t line)
{
    /* USER CODE BEGIN Error_Handler_Debug */
    /* User can add his own implementation to report the HAL error return state */
//    printf(pfile);
//    printf("\n%d\n", line);
//    while (1)
//        ;
    HAL_NVIC_SystemReset();
    /* USER CODE END Error_Handler_Debug */
}

#ifdef USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
    /* USER CODE BEGIN 6 */
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
    /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
