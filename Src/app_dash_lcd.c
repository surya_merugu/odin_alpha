/**
  *****************************************************************************
  * Title                 :   CONVERSION KIT
  * Filename              :   re_app_dash_lcd.c
  * Origin Date           :   14/08/2020
  * Compiler              :   ICCARM Complier
  * Hardware              :   FSTN LCD
  * Target                :   STM32F446RE-NUCLEO
  * Notes                 :   None
  *****************************************************************************
  * @attention
  *
  * <h2><center>&copy;Copyright (C) RACEnergy, Inc - All Rights Reserved.
  * </center></h2>
  *
  * Unauthorized copying of this file, via any medium is strictly prohibited
  * Proprietary and confidential
  *
  * Written by Team RACEnergy, AUG 2020
  *****************************************************************************
  */

/* Includes */
#include "app_dash_lcd.h"

uint16_t bat_error;

extern GPIO_InitTypeDef cs1_pin, cs2_pin, clk_pin, data_pin;
extern sys_t sys_info;

uint8_t range = 0;
bool blinkSoC_Flag;
static uint8_t Sys_Error_Codes[10];

FSTNLCD_data_t LcdDisplay;
const uint8_t lcd_mem_addr[2] = {0, 16};

CS_seg_t CS[2];

uint8_t n[25];
uint8_t z[56];

static uint8_t RE_num_to_7seg(uint8_t number);
static RE_StatusTypeDef RE_split_two_digit(uint8_t num, uint8_t addr);
static RE_StatusTypeDef RE_split_three_digit(uint16_t num, uint8_t addr);
static RE_StatusTypeDef RE_split_six_digit(uint32_t num, uint8_t addr);
static RE_StatusTypeDef RE_speed_to_graphic(uint8_t seg);
static RE_StatusTypeDef RE_efficiency_to_graphic(uint8_t seg);
static RE_StatusTypeDef RE_soc_to_graphic(uint8_t seg);
static RE_StatusTypeDef RE_update_err_code(void);
static RE_StatusTypeDef RE_update_speed(void);
static RE_StatusTypeDef RE_update_odo(void);
static RE_StatusTypeDef RE_update_speed_graphic(void);
static RE_StatusTypeDef RE_update_gear_mode(void);
static RE_StatusTypeDef RE_update_gear_dir(void);
static RE_StatusTypeDef RE_update_motor_temp(void);
static RE_StatusTypeDef RE_update_motor_fault(void);
static RE_StatusTypeDef RE_update_dock_latch_state(void);
static RE_StatusTypeDef RE_update_bat_temp(void);
static RE_StatusTypeDef RE_update_soc(void);
static RE_StatusTypeDef RE_update_soc_graphic(void);
static RE_StatusTypeDef RE_update_voltage(void);
static RE_StatusTypeDef RE_update_bat_fault(void);
static RE_StatusTypeDef RE_update_eff_in_graphic(void);
static RE_StatusTypeDef RE_update_range(void);
static RE_StatusTypeDef RE_write_to_display(void);
static RE_StatusTypeDef RE_load_lcd_data(void);
static uint8_t RE_calculate_eff(void);
static RE_StatusTypeDef RE_lcd_animate(void);
/**
  * @Brief RE_num_to_7seg
  * Convert the number to 7 segment
  * @Param number to be converted into 7 segment
  * @Retval Equivalent HEX
  */
static uint8_t RE_num_to_7seg(uint8_t number)
{
    switch (number)
    {
    case 0:
    {
        return ZERO;
        break;
    }
    case 1:
    {
        return ONE;
        break;
    }
    case 2:
    {
        return TWO;
        break;
    }
    case 3:
    {
        return THREE;
        break;
    }
    case 4:
    {
        return FOUR;
        break;
    }
    case 5:
    {
        return FIVE;
        break;
    }
    case 6:
    {
        return SIX;
        break;
    }
    case 7:
    {
        return SEVEN;
        break;
    }
    case 8:
    {
        return EIGHT;
        break;
    }
    case 9:
    {
        return NINE;
        break;
    }
    default:
    {
        return 0;
        break;
    }
    }
}

/**
  * @Brief RE_split_two_digit
  * Split a two digit number into individual digits
  * @Param num to be split into digits and convert to hex
  * @Param addr address to be written onto LCD memory
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_split_two_digit(uint8_t num, uint8_t addr)
{
    uint8_t factor = 100;
    uint8_t arr[2] = {0, 0};
    if (num == 100)
    {
        n[addr] = ZERO;
        n[addr + 1] = ZERO;
    }
    else
    {
        for (uint8_t i = 0; i < 2; i++)
        {
            factor = factor / 10;
            arr[i] = num / factor;
            num = num % factor;
            /* Convert the digit into 7 segment */
            arr[i] = RE_num_to_7seg(arr[i]);
            /* Assign the equivalent 7 segment to LCD memory address */
            n[addr + i] = arr[i];
        }
    }
    return RE_OK;
}

/**
  * @Brief RE_split_three_digit
  * Split a three digit number into individual digits
  * @Param num to be split into digits and convert to hex
  * @Param addr address to be written onto LCD memory
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_split_three_digit(uint16_t num, uint8_t addr)
{
    uint16_t factor = 1000;
    uint8_t arr[3] = {0, 0, 0};
    for (uint8_t i = 0; i < 3; i++)
    {
        factor = factor / 10;
        arr[i] = num / factor;
        num = num % factor;
        /* Convert the digit into 7 segment */
        arr[i] = RE_num_to_7seg(arr[i]);
        /* Assign the equivalent 7 segment to LCD memory address */
        n[addr + i] = arr[i];
    }
    return RE_OK;
}

/**
  * @Brief RE_split_six_digit
  * Split a six digit number into individual digits
  * @Param num to be split into digits and convert to hex
  * @Param addr address to be written onto LCD memory
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_split_six_digit(uint32_t num, uint8_t addr)
{
    uint32_t factor = 1000000;
    uint8_t arr[8] = {0, 0, 0, 0, 0, 0, 0, 0};
    for (uint8_t i = 0; i < 6; i++)
    {
        factor = factor / 10;
        arr[i] = num / factor;
        num = num % factor;
        /* Convert the digit into 7 segment */
        arr[i] = RE_num_to_7seg(arr[i]);
        /* Assign the equivalent 7 segment to LCD memory address */
        n[addr + i] = arr[i];
    }
    return RE_OK;
}

/**
  * @Brief RE_speed_to_graphic
  * Set speed on graphic meter
  * @Param seg segment to be displayed
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_speed_to_graphic(uint8_t seg)
{
    switch (seg)
    {
    case 0:
        z[54] = 0x10;
        for (uint8_t i = 0; i < 9; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 1:
        z[53] = 0x30;
        for (uint8_t i = 0; i < 8; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 2:
        z[52] = 0x70;
        for (uint8_t i = 0; i < 7; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 3:
        z[51] = 0xF0;
        for (uint8_t i = 0; i < 6; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 4:
        z[51] = 0XF0;
        z[50] = 0x08;
        for (uint8_t i = 0; i < 5; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 5:
        z[51] = 0XF0;
        z[49] = 0x0C;
        for (uint8_t i = 0; i < 4; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 6:
        z[51] = 0XF0;
        z[48] = 0x0E;
        for (uint8_t i = 0; i < 3; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 7:
        z[51] = 0XF0;
        z[47] = 0x0F;
        for (uint8_t i = 0; i < 2; i++)
        {
            z[45 + i] = 0;
        }
        break;
    case 8:
        z[51] = 0XF0;
        z[46] = 0x2F;
        z[45] = 0;
        break;
    default:
        z[51] = 0XF0;
        z[45] = 0x6F;
        break;
    }
    return RE_OK;
}

/**
  * @Brief RE_efficiency_to_graphic
  * Set efficiency on graphic meter
  * @Param seg segment to be displayed
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_efficiency_to_graphic(uint8_t seg)
{
    switch (seg)
    {
    case 0:
        z[24] = 0x80;
        for (uint8_t i = 1; i < 8; i++)
        {
            z[24 + i] = 0;
        }
        break;
    case 1:
        z[25] = 0xC0;
        for (uint8_t i = 2; i < 8; i++)
        {
            z[24 + i] = 0;
        }
        break;
    case 2:
        z[26] = 0xE0;
        for (uint8_t i = 3; i < 8; i++)
        {
            z[24 + i] = 0;
        }
        break;
    case 3:
        z[27] = 0xF0;
        for (uint8_t i = 4; i < 8; i++)
        {
            z[24 + i] = 0;
        }
        break;
    case 4:
        z[27] = 0XF0;
        z[28] = 0x01;
        z[29] = z[30] = z[31] = 0;
        break;
    case 5:
        z[27] = 0XF0;
        z[28] = z[29] = 0x01;
        z[30] = z[31] = 0;
        break;
    case 6:
        z[27] = 0XF0;
        z[28] = z[29] = z[30] = 0x01;
        z[31] = 0;
        break;
    case 7:
        z[27] = 0XF0;
        z[28] = z[29] = z[30] = z[31] = 0x01;
        break;
    }
    return RE_OK;
}

/**
  * @Brief Re_soc_to_graphic
  * Set SoC on graphic meter
  * @Param seg segment to be displayed
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_soc_to_graphic(uint8_t seg)
{
    switch (seg)
    {
    case 0:
        /* TODO: Blink first segment and also alert symbol */
        break;
    case 1:
        z[7] = 0x01;
        z[8] = z[9] = z[10] = 0;
        for (uint8_t i = 1; i < 8; i++)
        {
            z[7 + i] = 0;
        }
        break;
    case 2:
        z[8] = 0x03;
        z[7] = z[9] = z[10] = 0;
        for (uint8_t i = 2; i < 8; i++)
        {
            z[7 + i] = 0;
        }
        break;
    case 3:
        z[9] = 0x07;
        z[7] = z[8] = z[10] = 0;
        for (uint8_t i = 3; i < 8; i++)
        {
            z[7 + i] = 0;
        }
        break;
    case 4:
        z[10] = 0x0F;
        for (uint8_t i = 4; i < 8; i++)
        {
            z[7 + i] = 0;
        }
        break;
    case 5:
        z[7] = 0X0F;
        z[11] = 0x80;
        z[14] = z[12] = z[13] = 0;
        break;
    case 6:
        z[7] = 0X0F;
        z[11] = z[12] = 0xC0;
        z[13] = z[14] = 0;
        break;
    case 7:
        z[7] = 0X0F;
        z[11] = z[13] = z[12] = 0xE0;
        z[14] = 0;
        break;
    case 8:
        z[7] = 0X0F;
        z[14] = z[13] = z[12] = z[11] = 0XF0;
        break;
    }
    return RE_OK;
}

/**
  * @Brief RE_update_lcd_static_seg
  * updates static segments of LCD
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_update_lcd_static_seg(void)
{
    /* reset the screen */
    RE_lcd_reset();
    /* Initialise all NUMBERS to zero */
    for (uint8_t i = 0; i < 56; i++)
    {
        n[i] = ZERO;
    }
    n[21] = n[22] = n[23] = 0;
    for (uint8_t i = 0; i <= 7; i++)
    {
        z[i + 7] = 0;
        z[i + 24] = 0;
    }
    for (uint8_t i = 0; i < 10; i++)
    {
        z[i + 45] = 0;
    }
    /** Error,Lock symbol */
    z[2] = 0x04;
    z[3] = 0x00;

    /** Speed */
    z[44] = 0x10;
    z[43] = 0x01;
    /* Gear */
    /** Gear is not required */
    z[55] = 0x00;
    /** Motor */
    z[39] = 0x00; /** *C */
    z[38] = 0x00; /** Temperature symbol */
    z[36] = 0x10;
    z[37] = 0x20;
    /** Battery */
    z[5] = z[4] = 0x01;
    z[0] = z[18] = z[17] = 0x01;
    z[15] = 0x08;
    z[16] = 0;
    /* Range */
    z[21] = z[19] = 1;
    /* Odo */
    z[23] = z[22] = 0x01;
    z[32] = 0x08;
    z[44] = 0x10;
    z[21] = z[43] = z[19] = 0x01;
    z[1] = 0x08;
    z[34] = 0x04;
    z[33] = 0x02;
    return RE_OK;
}

/**
  * @Brief RE_update_err_code
  * updates error codes on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_err_code(void)
{
    /* Dock fault representation [1:4] */
    static uint8_t dock_seg[4] = {0x80,
                                  0x04,
                                  0x20,
                                  0x02};

    z[2] = 0x04;
#if 0
    RE_split_three_digit(LcdDisplay.err_code, 1);
#endif
    n[2] = 0;
    n[3] = 0;
    /* n[2] : Dock CAN communication status */
    for (uint8_t i = 0; i < 4; i++)
    {
        if(i == 0 || i == 1)
        {
          if ((sys_info.dock_status >> i) & 0x01)
          {
              n[2] |= dock_seg[i];
          }
          if ((sys_info.packs_count >> i) & 0x01)
          {
              n[2] |= dock_seg[i + 2];
          }
        }
        else if(i == 2 || i == 3)
        {
          if ((sys_info.dock_status >> i) & 0x01)
          {
              n[3] |= dock_seg[i - 2];
          }
          if ((sys_info.packs_count >> i) & 0x01)
          {
              n[3] |= dock_seg[i];
          }
        }
    }
    /* n[3] : Dock pack detection status */
    return RE_OK;
}

/**
  * @Brief RE_update_dock_latch_state
  * updates dock latch status on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_dock_latch_state(void)
{
    if (sys_info.latch == 0)
    {
        z[3] = 0; /** Closed */
    }
    else
    {
        z[3] = 2; /** Opened */
    }
    return RE_OK;
}

/**
  * @Brief RE_update_speed
  * updates speed on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_speed(void)
{
    RE_split_two_digit(sys_info.speed, 4);
    return RE_OK;
}

/**
  * @Brief RE_update_speed_graphic
  * updates speed graphics on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_speed_graphic(void)
{
    uint8_t kmph = 0;
    kmph = sys_info.speed / 6;
    for (uint8_t i = 0; i < 10; i++)
    {
        z[45 + i] = 0x0;
    }
    if (sys_info.speed > 0)
    {
        RE_speed_to_graphic(kmph);
    }
    return RE_OK;
}

/**
  * @Brief RE_update_odo
  * updates ODO on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_odo(void)
{
    RE_split_six_digit(sys_info.ODO_KM, 15);
    return RE_OK;
}

/**
  * @Brief RE_update_gear_mode
  * updates gear mode on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_gear_mode(void)
{
#if 0
    switch (LcdDisplay.gear)
    {
        case 1:
            n[23] = LOW;
            break;
        case 2:
            n[23] = AUTO;
            break;
        case 3:
            n[23] = HIGH;
            break;
    }
#endif
    return RE_OK;
}

/**
  * @Brief RE_update_motor_temp
  * updates motor temperature on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_motor_temp(void)
{
    RE_split_two_digit(LcdDisplay.motor_temp, 21);
    return RE_OK;
}

/**
  * @Brief RE_update_gear_dir
  * updates gear direction on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_gear_dir(void)
{
    switch (sys_info.gear_mode)
    {
    case 0:
        /* Drive */
        z[40] = 0x8;
        z[41] = z[42] = 0;
        break;
    case 1:
        /* Reverse */
        z[42] = 0x2;
        z[40] = z[41] = 0;
        break;
    case 2:
        /* Neutral */
        z[41] = 0x4;
        z[40] = z[42] = 0;
        break;
    default:
        break;
    }
    return RE_OK;
}

/**
  * @Brief RE_update_motor_fault
  * updates motor fault on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_motor_fault(void)
{
    z[36] = 0x10; /* Motor symbol */
    if (sys_info.t_fuse != 0)
    {
        /* Thermal cutoff */
        z[37] = 0x20;
    }
    else
    {
        /* No thermal cutoff */
        z[37] = 0x00;
    }
    return RE_OK;
}

/**
  * @Brief RE_update_bat_temp
  * updates battery temperature on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_bat_temp(void)
{
    RE_split_two_digit(sys_info.bat_temp, 6);
    return RE_OK;
}

/**
  * @Brief RE_update_soc
  * updates Soc on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_soc(void)
{
    //  LcdDisplay.bat_soc = LogData.bat[2].soc;
    if (sys_info.bat_soc > 99)
    {
        z[16] = 0x01;
        //        sys_info.bat_soc = 0;
    }
    else
    {
        z[16] = 0;
    }
    RE_split_two_digit(sys_info.bat_soc, 11);
    return RE_OK;
}

/**
  * @Brief RE_update_soc_graphic
  * updates Soc graphics on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_soc_graphic(void)
{
    uint8_t soc = 0;
    soc = sys_info.bat_soc / 12;
    for (uint8_t i = 0; i < 8; i++)
    {
        z[7 + i] = 0x0;
    }
    if (sys_info.bat_soc < 20)
    {
        if (blinkSoC_Flag)
        {
            blinkSoC_Flag = false;
            soc = 1;
        }
        else
        {
            blinkSoC_Flag = true;
            soc = 0;
        }
    }
    RE_soc_to_graphic(soc);
    return RE_OK;
}

/**
  * @Brief RE_update_voltage
  * updates volatge on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_voltage(void)
{
    RE_split_three_digit((sys_info.bat_volt), 8);
    return RE_OK;
}

/**
  * @Brief RE_update_bat_fault
  * updates battery fault on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_bat_fault(void)
{
    if (bat_error == 0)
    {
        z[6] = 0;
    }
    else
    {
        z[6] = 0x04;
    }
    return RE_OK;
}

/**
  * @Brief RE_update_eff_in_graphic
  * updates efficiency graphics on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_eff_in_graphic(void)
{
    uint8_t seg = 0;
    //    sys_info.efficiency = RE_calculate_eff();
    if (sys_info.efficiency <= 30)
    {
        seg = 0;
    }
    else if (sys_info.efficiency > 30 && sys_info.efficiency <= 40)
    {
        seg = 1;
    }
    else if (sys_info.efficiency > 40 && sys_info.efficiency <= 60)
    {
        seg = 2;
    }
    else if (sys_info.efficiency > 60 && sys_info.efficiency <= 65)
    {
        seg = 3;
    }
    else if (sys_info.efficiency > 65 && sys_info.efficiency <= 70)
    {
        seg = 4;
    }
    else if (sys_info.efficiency > 70 && sys_info.efficiency <= 80)
    {
        seg = 5;
    }
    else if (sys_info.efficiency > 80 && sys_info.efficiency <= 90)
    {
        seg = 6;
    }
    else if (sys_info.efficiency > 90)
    {
        seg = 7;
    }
    RE_efficiency_to_graphic(seg);
    return RE_OK;
}

/**
  * @Brief RE_update_range
  * updates range on LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_update_range(void)
{
    //    range = (LcdDisplay.bat_soc * 3) / 4;
    // range = (LcdDisplay.bat_soc);
    RE_split_two_digit(sys_info.range, 13);
    return RE_OK;
}

/**
  * @Brief RE_write_to_display
  * This function writes data to LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_write_to_display(void)
{
    RE_load_lcd_data();
    /* CS1 */
    RE_lcd_write_data(lcd_mem_addr[0], CS[0].seg[0], cs1_pin.Pin);
    RE_lcd_write_data(lcd_mem_addr[1], CS[0].seg[1], cs1_pin.Pin);
    /* CS2 */
    RE_lcd_write_data(lcd_mem_addr[0], CS[1].seg[0], cs2_pin.Pin);
    RE_lcd_write_data(lcd_mem_addr[1], CS[1].seg[1], cs2_pin.Pin);

    return RE_OK;
}

/**
  * @Brief RE_load_lcd_data
  * This function loads data to LCD
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_load_lcd_data(void)
{

    CS[0].seg[0] = ((~(~(n[22] | z[39]) & 0xFFFFFFFFFFFFFFFF)) |
                    (~(~(n[21] | z[38]) & 0xFFFFFFFFFFFFFFFF) << 8) |
                    (~(~(n[14] | z[21]) & 0xFFFFFFFFFFFFFFFF) << 16) |
                    (~(~(n[13] | z[20]) & 0xFFFFFFFFFFFFFFFF) << 24) |
                    (~(~(n[23]) & 0xFFFFFFFFFFFFFFFF) << 32) |
                    (~(~(n[5] | z[43]) & 0xFFFFFFFFFFFFFFFF) << 40) |
                    (~(~(n[4]) & 0xFFFFFFFFFFFFFFFF) << 48) |
                    (~(~(n[3]) & 0xFFFFFFFFFFFFFFFF) << 56));

    CS[0].seg[1] = ((~(~(n[2]) & 0xFFFFFFFFFFFFFFFF)) |
                    (~(~(n[1]) & 0xFFFFFFFFFFFFFFFF) << 8) |
                    (~(~(z[51] | z[52] | z[53] | z[54] | z[1] | z[2] | z[3] | z[19]) & 0xFFFFFFFFFFFFFFFF) << 16) |
                    (~(~(z[45] | z[46] | z[44] | z[50] | z[49] | z[48] | z[47]) & 0xFFFFFFFFFFFFFFFF) << 24) |
                    (~(~(z[35] | z[37] | z[36] | z[42] | z[41] | z[40] | z[55]) & 0xFFFFFFFFFFFFFFFF) << 32));

    CS[1].seg[0] = ((~(~(n[20] | z[23]) & 0xFFFFFFFFFFFFFFFF)) |
                    (~(~(n[19] | z[31]) & 0xFFFFFFFFFFFFFFFF) << 8) |
                    (~(~(n[18] | z[30]) & 0xFFFFFFFFFFFFFFFF) << 16) |
                    (~(~(n[17] | z[29]) & 0xFFFFFFFFFFFFFFFF) << 24) |
                    (~(~(n[16] | z[28]) & 0xFFFFFFFFFFFFFFFF) << 32) |
                    (~(~(n[15] | z[22]) & 0xFFFFFFFFFFFFFFFF) << 40) |
                    (~(~(n[7] | z[5]) & 0xFFFFFFFFFFFFFFFF) << 48) |
                    (~(~(n[6] | z[4]) & 0xFFFFFFFFFFFFFFFF) << 56));

    CS[1].seg[1] = ((~(~(n[11] | z[16]) & 0xFFFFFFFFFFFFFFFF)) |
                    (~(~(n[12] | z[17]) & 0xFFFFFFFFFFFFFFFF) << 8) |
                    (~(~(n[8]) & 0xFFFFFFFFFFFFFFFF) << 16) |
                    (~(~(n[9] | z[0]) & 0xFFFFFFFFFFFFFFFF) << 24) |
                    (~(~(n[10] | z[18]) & 0xFFFFFFFFFFFFFFFF) << 32) |
                    (~(~(z[11] | z[12] | z[13] | z[14] | z[10] | z[9] | z[8] | z[7]) & 0xFFFFFFFFFFFFFFFF) << 40) |
                    (~(~(z[15] | z[6]) & 0xFFFFFFFFFFFFFFFF) << 48) |
                    (~(~(z[24] | z[25] | z[26] | z[27] | z[32] | z[34] | z[33]) & 0xFFFFFFFFFFFFFFFF) << 56));

    return RE_OK;
}

/**
  * @Brief RE_calculate_eff
  * This function calculates the efficiency
  * @Param None
  * @Retval Exit status
  */
static uint8_t RE_calculate_eff(void)
{
    //
    //    for (uint8_t i = 0; i < 4; i++)
    //    {
    //        if (LogData.bat[i].current > 0)
    //        {
    //            LcdDisplay.current_in_amp[i] = (65535 - LogData.bat[i].current) / 1000;
    //        }
    //        else
    //        {
    //            LcdDisplay.current_in_amp[i] = 0;
    //        }
    //    }
    //    /* 3800 mech rpm = 150 elec rpm */
    /* 6150 mech rpm = 509 elec rpm */
    return (int)(LcdDisplay.speed * 100) / (LcdDisplay.current_in_amp);
}

/**
  * @Brief RE_lcd_animate
  * This function starts the LCD animation
  * @Param None
  * @Retval Exit status
  */
static RE_StatusTypeDef RE_lcd_animate(void)
{
    RE_update_lcd_static_seg();
    RE_write_to_display();
    /* Increment the segment */
    for (uint8_t i = 0; i < 8; i++)
    {
        if (i == 0)
        {
            RE_speed_to_graphic(0);
        }
        else if (i == 1)
        {
            RE_speed_to_graphic(2);
        }
        else if (i == 2)
        {
            RE_speed_to_graphic(4);
        }
        else
        {
            RE_speed_to_graphic(i + 2);
        }
        RE_efficiency_to_graphic(i);
        RE_soc_to_graphic(i);
        RE_write_to_display();
        HAL_Delay(75);
    }
    /* Decrement the segment */
    HAL_Delay(150);
    for (uint8_t i = 7; i > 0; i--)
    {
        if (i == 1)
        {
            RE_speed_to_graphic(0);
        }
        else if (i == 2)
        {
            RE_speed_to_graphic(2);
        }
        else if (i == 3)
        {
            RE_speed_to_graphic(4);
        }
        else
        {
            RE_speed_to_graphic(i + 1);
        }
        RE_efficiency_to_graphic(i - 1);
        RE_soc_to_graphic(i - 1);
        RE_write_to_display();
        HAL_Delay(75);
    }
    return RE_OK;
}

/**
  * @Brief RE_RefreshScreen
  * This function Refreshes the LCD
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_LCD_ON(void)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET); /* Power on LCD */
    HAL_Delay(5);
    RE_lcd_animate();
    return RE_OK;
}

/**
  * @Brief RE_RefreshScreen
  * This function Refreshes the LCD
  * @Param None
  * @Retval Exit status
  */
RE_StatusTypeDef RE_RefreshScreen(void)
{
    /* Error code and number */
    z[2] = 0x00;
    n[1] = n[2] = n[3] = 0x00;
    RE_update_err_code();
    RE_update_dock_latch_state();
    RE_update_speed();
    RE_update_speed_graphic();
    RE_update_odo();
#if 0
       RE_update_gear_mode();
       RE_update_motor_temp();
#endif
    RE_update_gear_dir();
    RE_update_motor_fault();
    RE_update_bat_temp();
    RE_update_soc();
    RE_update_soc_graphic();
    RE_update_voltage();
    RE_update_bat_fault();
    RE_update_eff_in_graphic();
    RE_update_range();

    RE_write_to_display();
    return RE_OK;
}

void RE_DisplayingErrorCode(uint8_t *Error_code)
{
    uint8_t index = 0;
    uint8_t Sys_Error_Codes_Len = strlen((char *)Sys_Error_Codes);
    if (Sys_Error_Codes_Len == 0)
    {
        index = 0;
    }
    else
    {
        index = Sys_Error_Codes_Len + 1;
    }
    /*Error_code[0] : 0x01 - Raised Frigg Error code, 0x02 - Suppressed Frigg Error code
      Error_code[1] : bit0 : g2g error, bit1 : missing pack error, bit2 : tamper error, bit3 : latch error
      bit4 : backup bat low error, bit5 : motor over temp error, bit6 : bat over temp error, bit7 : speed sensor error*/
    switch (Error_code[0])
    {
    case 0x01: /*Raised Frigg Error code*/
        for (uint8_t i = 0; i < 8; i++)
        {
            if ((Error_code[1] >> i) & 0x01)
            {
                switch (i)
                {
                case 0:
                    Sys_Error_Codes[index] = G2G_ERROR_CODE;
                    index++;
                    break;
                case 1:
                    Sys_Error_Codes[index] = MISSING_PACKS_ERROR_CODE;
                    index++;
                    break;
                case 2:
                    Sys_Error_Codes[index] = TAMPER_ERROR_CODE;
                    index++;
                    break;
                case 3:
                    Sys_Error_Codes[index] = LATCH_ERROR_CODE;
                    index++;
                    break;
                case 4:
                    Sys_Error_Codes[index] = BK_BAT_LOW_ERROR_CODE;
                    index++;
                    break;
                case 5:
                    Sys_Error_Codes[index] = MOTOR_OVER_TEMP_ERROR_CODE;
                    index++;
                    break;
                case 6:
                    Sys_Error_Codes[index] = BAT_OVER_TEMP_ERROR_CODE;
                    index++;
                    break;
                case 7:
                    Sys_Error_Codes[index] = SPEED_SENSOR_ERROR_CODE;
                    index++;
                    break;
                }
            }
        }
        break;
    case 0x02: /*Suppressed Frigg Error code*/
        for (uint8_t i = 0; i < 8; i++)
        {
            if ((Error_code[1] >> i) & 0x01)
            {
                switch (i)
                {
                case 0:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == G2G_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 1:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == MISSING_PACKS_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 2:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == TAMPER_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 3:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == LATCH_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 4:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == BK_BAT_LOW_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 5:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == MOTOR_OVER_TEMP_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 6:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == BAT_OVER_TEMP_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                case 7:
                    for (uint8_t i = 0; i < 11; i++)
                    {
                        if (Sys_Error_Codes[i] == SPEED_SENSOR_ERROR_CODE)
                        {
                            Sys_Error_Codes[i] = 0;
                            break;
                        }
                    }
                    break;
                }
            }
        }
        break;
    }
    uint8_t buffer = 0;
    for (uint8_t i = 0; i < 11; ++i)
    {
        for (uint8_t j = i + 1; j < 11; ++j)
        {
            if (Sys_Error_Codes[i] < Sys_Error_Codes[j])
            {
                buffer = Sys_Error_Codes[i];
                Sys_Error_Codes[i] = Sys_Error_Codes[j];
                Sys_Error_Codes[j] = buffer;
            }
        }
    }
    Sys_Error_Codes_Len = strlen((char *)Sys_Error_Codes);
    LcdDisplay.err_code = Sys_Error_Codes[Sys_Error_Codes_Len - 1];
}

/************************ (C) COPYRIGHT RACEnergy **********END OF FILE********/