/**
  ******************************************************************************
  * @file           : gpio.c
  * @brief          : GPIO peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "gpio.h"

extern sys_t sys_info;
bool Ec20_Reset_Flag = false;

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
void RE_GPIO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOC, LCD_CLK_Pin | LCD_DI_Pin | CAN1_STB_Pin | EC20_RESET_Pin | EC20_IO_Pin | LED_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOB, LCD_CTRL_Pin | CAN1_EN_Pin | BUZZ_Pin | LCD_CS1_Pin | LCD_CS2_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOA, EC20_WAKE_Pin | EC20_PWR_Pin, GPIO_PIN_RESET);

    /*Configure GPIO pins : LCD_CLK_Pin LCD_DI_Pin CAN1_STB_Pin EC20_RESET_Pin LED_Pin */
    GPIO_InitStruct.Pin = LCD_CLK_Pin | LCD_DI_Pin | EC20_RESET_Pin | LED_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /*Configure GPIO pins : PC15 PC4 PC5 PC6 PC10 PC12 */
    GPIO_InitStruct.Pin = GPIO_PIN_15 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_10 | GPIO_PIN_12;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

#if 0
    /*Configure GPIO pins : GEAR_N_Pin GEAR_R_Pin CAN1_ERR_Pin */
    GPIO_InitStruct.Pin = GEAR_N_Pin | GEAR_R_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
 
#endif
  
    /*Configure GPIO pins : GEAR_N_Pin GEAR_R_Pin CAN1_ERR_Pin */
    GPIO_InitStruct.Pin = GEAR_N_Pin | GEAR_R_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    
    GPIO_InitStruct.Pin  = GPIO_PIN_7;
    GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : PA0 PA4 PA5 PA6 PA7 */
    GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /*Configure GPIO pins : PB10 PB12 PB13 PB15 */
    GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_15;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : LCD_CTRL_Pin CAN1_EN_Pin BUZZ_Pin LCD_CS1_Pin LCD_CS2_Pin */
    GPIO_InitStruct.Pin = LCD_CTRL_Pin | BUZZ_Pin | LCD_CS1_Pin | LCD_CS2_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

    /*Configure GPIO pins : EC20_WAKE_Pin EC20_PWR_Pin */
    GPIO_InitStruct.Pin = EC20_WAKE_Pin | EC20_PWR_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    
    /*Configure GPIO pins : EC20_IO_Pin*/
    GPIO_InitStruct.Pin = EC20_IO_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

#if 0
    /* EXTI interrupt init*/
    HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);

    HAL_NVIC_SetPriority(EXTI1_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);
#endif

    HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);
    
    HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    
    /* FIXME: EC20 power control */
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, EC20_IO_Pin, GPIO_PIN_RESET);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    // GPIO_PinState io_status = GPIO_PIN_RESET;
    // uint8_t io_event[2] = {0};
    switch (GPIO_Pin)
    {
    case CAN1_ERR_Pin:
        __NOP();
        break;
#if 0
    case GEAR_DRV:
    case GEAR_REV:
        sys_info.gear_mode = (uint8_t)(GPIOC->IDR & 0x03);
        break;
#endif
    case GPIO_PIN_7:
        Ec20_Reset_Flag = false;
        break;    
    default:
        break;
    }
}
