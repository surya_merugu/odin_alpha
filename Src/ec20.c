/**
  ******************************************************************************
  * @file           : ec20.c
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "ec20.h"

Queue_t *p_UartRxQueue;
Queue_t *p_UartTxQueue;
UART_HandleTypeDef huart1;
bool Ec20_pwrdown_Req_Flag = false;

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
void RE_USART1_UART_Init(void)
{

    /* USER CODE BEGIN USART1_Init 0 */

    /* USER CODE END USART1_Init 0 */

    /* USER CODE BEGIN USART1_Init 1 */

    /* USER CODE END USART1_Init 1 */
    huart1.Instance = USART1;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart1) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /* USER CODE BEGIN USART1_Init 2 */
    p_UartRxQueue = RE_CreateQueue(UartRxQueueItems, UartRxQueueItemsLen);
    p_UartTxQueue = RE_CreateQueue(UartTxQueueItems, UartTxQueueItemsLen);
    /* USER CODE END USART1_Init 2 */
    RE_EC20_PowerUp();
}

void RE_EC20_PowerUp(void)
{
    HAL_GPIO_WritePin(EC20_PWR_GPIO_Port, EC20_PWR_Pin, GPIO_PIN_SET); /* Logic 1: power up; 0:power dpwn  */
    // HAL_GPIO_WritePin(EC20_WAKE_GPIO_Port, EC20_WAKE_Pin, GPIO_PIN_SET);
}

void RE_EC20_PowerDown(void)
{
    HAL_GPIO_WritePin(EC20_PWR_GPIO_Port, EC20_PWR_Pin, GPIO_PIN_RESET); /* Logic 1: power up; 0:power dpwn  */
    // HAL_GPIO_WritePin(EC20_WAKE_GPIO_Port, EC20_WAKE_Pin, GPIO_PIN_RESET);
}

void RE_EC20_Reset(void)
{
    HAL_GPIO_WritePin(EC20_RESET_GPIO_Port, EC20_RESET_Pin, GPIO_PIN_SET);
    HAL_Delay(300); /* 150ms ~ 460ms logic high pulse */
    HAL_GPIO_WritePin(EC20_RESET_GPIO_Port, EC20_RESET_Pin, GPIO_PIN_RESET);
}

void RE_EC20_ReadMsg(uint8_t *p_Ec20RxMsg)
{
    uint8_t QueueItem[13];
    for (uint8_t i = 6, j = 1; i >= 0 && j < 8; i--, j++)
    {
        if ((p_Ec20RxMsg[14] >> i) & 0x01)
        {
            p_Ec20RxMsg[j] = p_Ec20RxMsg[j] - 1;
        }
    }
    for (uint8_t i = 6, j = 8; i >= 1 && j < 14; i--, j++)
    {
        if ((p_Ec20RxMsg[15] >> i) & 0x01)
        {
            p_Ec20RxMsg[j] = p_Ec20RxMsg[j] - 1;
        }
    }
    memcpy(QueueItem, &p_Ec20RxMsg[1], 13);
    uint32_t can_id = (uint32_t)(QueueItem[0] << 24 | QueueItem[1] << 16 | QueueItem[2] << 8 | QueueItem[3]);
    if(can_id == 0x00D00001)
    {
//        uint8_t Ec20_pwrdown_event[2];
        if(QueueItem[5] == 0x00)
        {
            Ec20_pwrdown_Req_Flag = true;
        }
        else
        {
            Ec20_pwrdown_Req_Flag = false;
            RE_EC20_PowerDown();
        }
        HAL_GPIO_WritePin(GPIOC, EC20_IO_Pin, GPIO_PIN_RESET);
    }
    else if(can_id == 0x00C40002)
    {
        uint8_t buffer[2] = {0x01, 0x01}; /* DLC, 1: Unlock latch */
        RE_CAN_Format_TxMsg(RX_NODE_FRIGG, 00, 0x02, buffer);
    }
    __NOP();
    Ringbuf_UART1_Free();
}

void RE_EC20_WriteMsg(uint8_t *p_Ec20TxMsg)
{
    uint8_t UartTx_Buffer[17];
    memset(UartTx_Buffer, 0x00, 17);
    UartTx_Buffer[0] = 0x23;
    UartTx_Buffer[16] = 0x40;
    UartTx_Buffer[14] |= (1 << 7);
    UartTx_Buffer[15] |= (1 << 7);
    for (uint8_t i = 0; i < 13; i++)
    {
        if ((p_Ec20TxMsg[i] == 0x23) | (p_Ec20TxMsg[i] == 0x40))
        {
            p_Ec20TxMsg[i] = p_Ec20TxMsg[i] + 1;
            if (i < 7)
            {
                UartTx_Buffer[14] |= (1 << (6 - i));
            }
            else
            {
                UartTx_Buffer[15] |= (1 << (13 - i));
            }
        }
    }
    memcpy(&UartTx_Buffer[1], p_Ec20TxMsg, 13);
    if (HAL_UART_Transmit(&huart1, UartTx_Buffer, 17, 100) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
}