/**
  ******************************************************************************
  * @file           : queue.c
  * @brief          : Queue implementation
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "queue.h"
#include "limits.h"
#include "stdlib.h"

#if 0
static uint8_t RE_Queue_FirstItem(Queue_t *p_queue_t);
static uint8_t RE_Queue_LastItem(Queue_t *p_queue_t);
#endif

/* Function to create a queue of given capacity and number of elements(Queue membersz) */
Queue_t *RE_CreateQueue(uint8_t no_of_items, uint8_t item_len)
{
    Queue_t *p_queue_t;
    p_queue_t = (Queue_t *)malloc(sizeof(Queue_t));
    p_queue_t->capacity = item_len * no_of_items;
    p_queue_t->first = 0;
    p_queue_t->size = 0;
    p_queue_t->last = p_queue_t->capacity - 1;
    p_queue_t->p_array = (uint8_t *)malloc(p_queue_t->capacity * sizeof(uint8_t));
    return p_queue_t;
}

/* Function to delete a given queue */
void RE_DeleteQueue(Queue_t *p_queue_t)
{
    free(p_queue_t);
}

/* Queue is full when size becomes equal to the capacity */
uint8_t RE_IsQueueFull(Queue_t *p_queue_t)
{
    return (p_queue_t->size >= p_queue_t->capacity);
}

/* Queue is empty when size is zero */
uint8_t RE_IsQueueEmpty(Queue_t *p_queue_t)
{
    if (p_queue_t->size == 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

/* Function to add an item to the queue. */
/* len: Length of the item */
uint8_t RE_EnQueue(Queue_t *p_queue_t, uint8_t *p_item, uint8_t len)
{
    if (RE_IsQueueFull(p_queue_t))
    {
        return 1;
    }
    for (uint8_t i = 0; i < len; i++)
    {
        p_queue_t->last = (p_queue_t->last + 1) % p_queue_t->capacity;
        p_queue_t->p_array[(p_queue_t->last)] = *(p_item + i);
        p_queue_t->size = p_queue_t->size + 1;
    }
    return 0;
}

/* Function to remove an item from queue */
uint8_t *RE_DeQueue(Queue_t *p_queue_t, uint8_t len)
{
//    if (RE_IsQueueEmpty(p_queue_t))
//    {
//        return INT_MIN;
//    }
    uint8_t *p_item = &(p_queue_t->p_array[p_queue_t->first]);
    p_queue_t->first = (p_queue_t->first + len) % (p_queue_t->capacity);
    p_queue_t->size = p_queue_t->size - len;
    return p_item;
}
#if 0
// Function to get first item of the queue
static uint8_t RE_Queue_FirstItem(Queue_t *p_queue_t)
{
    if (RE_IsQueueEmpty(p_queue_t))
    {
        return INT_MIN;
    }
    return p_queue_t->p_array[p_queue_t->first];
}

// Function to get last item of the queue
static uint8_t RE_Queue_LastItem(Queue_t *p_queue_t)
{
    if (RE_IsQueueEmpty(p_queue_t))
    {
        return INT_MIN;
    }
    return p_queue_t->p_array[p_queue_t->last];
}
#endif