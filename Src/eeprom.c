/**
  ******************************************************************************
  * @file           : eeprom.c
  * @brief          : EEPROM peripheral
  ******************************************************************************
  * @attention
  *
  ******************************************************************************
  */
#include "eeprom.h"
#include "limits.h"

I2C_HandleTypeDef hi2c1;

static __IO uint32_t t_ms;
static __IO uint8_t rx_cplt = 0;

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
void RE_I2C1_Init(void)
{

    /* USER CODE BEGIN I2C1_Init 0 */

    /* USER CODE END I2C1_Init 0 */

    /* USER CODE BEGIN I2C1_Init 1 */

    /* USER CODE END I2C1_Init 1 */
    hi2c1.Instance = I2C1;
    hi2c1.Init.ClockSpeed = 100000;
    hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
    hi2c1.Init.OwnAddress1 = 0;
    hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2 = 0;
    hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    /* USER CODE BEGIN I2C1_Init 2 */

    /* USER CODE END I2C1_Init 2 */
}

void HAL_I2C_MemTxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    __NOP();
}

void HAL_I2C_MemRxCpltCallback(I2C_HandleTypeDef *hi2c)
{
    rx_cplt = 0x01;
    __NOP();
}

void RE_EEPROM_Write(uint8_t *p_data, uint8_t len, uint16_t mem_address)
{
    while (HAL_I2C_GetState(&hi2c1) != HAL_I2C_STATE_READY)
    {
    }
    while ((HAL_GetTick() - t_ms) < 6)
    {
    }
    if (HAL_I2C_Mem_Write_DMA(&hi2c1, EEPROM_RW_ADDR, mem_address, MEM_ADD_SIZE, p_data, len) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    t_ms = HAL_GetTick();
}

void RE_EEPROM_Read(uint8_t *p_data, uint8_t len, uint16_t mem_address)
{
    rx_cplt = 0x00;
    while ((HAL_GetTick() - t_ms) < 6)
    {
    }
    if (HAL_I2C_Mem_Read_DMA(&hi2c1, EEPROM_RW_ADDR, mem_address, MEM_ADD_SIZE, p_data, len) != HAL_OK)
    {
        RE_Error_Handler(__FILE__, __LINE__);
    }
    t_ms = HAL_GetTick();
    /* Wait for the data to be received */
    while (!rx_cplt)
    {
    }
}
