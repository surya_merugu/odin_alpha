#include "task.h"

Queue_t *p_Event_Queue;
Queue_t *p_Task_Queue;

extern Queue_t *p_CanRxQueue;
extern Queue_t *p_CanTxQueue;
extern Queue_t *p_UartRxQueue;
extern Queue_t *p_UartTxQueue;

extern sys_t sys_info;

bool Queue_uart_msgs;
bool Reset_Flag = false;

static void RE_ProcessEvents(uint8_t *p_event);
static void RE_ProcessTasks(uint8_t *p_task);

void RE_Create_EVT_Queue(void)
{
    p_Event_Queue = RE_CreateQueue(EVT_QUEUE_LEN, EVT_QUEUE_ITEM_LEN);
}

void RE_Create_TASK_Queue(void)
{
    p_Task_Queue = RE_CreateQueue(TASK_QUEUE_LEN, TASK_QUEUE_ITEM_LEN);
}

bool RE_Task_Pending(void)
{
    bool ret_val = false;
    if (RE_IsQueueEmpty(p_Event_Queue) != 0)
    {
        ret_val |= true;
        uint8_t *p_event = RE_DeQueue(p_Event_Queue, EVT_QUEUE_ITEM_LEN);
        RE_ProcessEvents(p_event);
    }
    if (RE_IsQueueEmpty(p_Task_Queue) != 0)
    {
        ret_val |= true;
        uint8_t *p_task = RE_DeQueue(p_Task_Queue, TASK_QUEUE_ITEM_LEN);
        RE_ProcessTasks(p_task);
    }
    if (RE_IsQueueEmpty(p_CanRxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_CanRxMsg = RE_DeQueue(p_CanRxQueue, RxQueueItemLen);
        if (Queue_uart_msgs)
        {
            RE_EnQueue(p_UartTxQueue, p_CanRxMsg, UartTxQueueItemsLen);
        }
        /* p_CanRxMsg[0:3] = { 29-bit CAN ID } */
        /* p_CanRxMsg[4] = DLC */
        uint8_t msg_id = p_CanRxMsg[3];
        uint8_t dock_id = p_CanRxMsg[2];
        uint8_t peripheral_id = (p_CanRxMsg[1] & 0x0F);
        uint8_t node_id = (p_CanRxMsg[1] >> 4);
        uint8_t mode = 0;
        switch (node_id)
        {
        case 0x0A:                                                /* Msg from Battery */
            if(p_CanRxMsg[3] == 0x01)
            {
                RE_EnQueue(p_UartTxQueue, p_CanRxMsg, UartTxQueueItemsLen);
            }
            RE_CAN_Read_Bat_Msg(dock_id, msg_id, &p_CanRxMsg[4]); /* p_CanRxMsg[4] = DLC */
            break;
        case 0x0B:                                         /* From FRIGG */
            if(p_CanRxMsg[3] == 0x02)
            {
                RE_EnQueue(p_UartTxQueue, p_CanRxMsg, UartTxQueueItemsLen);
            }
            else if(p_CanRxMsg[3] == 0x03)
            {
                RE_EnQueue(p_UartTxQueue, p_CanRxMsg, UartTxQueueItemsLen);
            }
            else
            {
                __NOP();
            }
            RE_CAN_Read_Frigg_Msg(msg_id, &p_CanRxMsg[4]); /* p_CanRxMsg[4] = DLC */
            break;
        case 0x0C:
            switch (p_CanRxMsg[2] & 0x0F)
            {
            case 0x00: /* Write/Read to/from EEPROM [0x00C10001] */
                mode = (p_CanRxMsg[4] != 0) ? EEPROM_WRITE : EEPROM_READ;
                /* TODO: */
                //        RE_CAN_Write_EEPROM(&p_data[2]);
                /* code */
                break;
            default:
                break;
            }
            break;
        default:
            break;
        }
    }
    if (RE_IsQueueEmpty(p_CanTxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_CanTxMsg = RE_DeQueue(p_CanTxQueue, TxQueueItemLen);
        if (p_CanTxMsg != 0)
        {
            RE_CAN_WriteMsg(p_CanTxMsg);
        }
    }
    if (RE_IsQueueEmpty(p_UartRxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_UartQueueMsg = RE_DeQueue(p_UartRxQueue, UartRxQueueItemsLen);
        if(p_UartQueueMsg != 0)
        {
            RE_EC20_ReadMsg(p_UartQueueMsg);
        }
    }
    if (RE_IsQueueEmpty(p_UartTxQueue) != 0)
    {
        ret_val |= true;
        uint8_t *p_UartQueueMsg = RE_DeQueue(p_UartTxQueue, UartTxQueueItemsLen);
        if(p_UartQueueMsg != 0)
        {
            RE_EC20_WriteMsg(p_UartQueueMsg);
        }
    }
    if (uart2RxCmplt)
    {
        uart2RxCmplt = false;
        UART2_HandleData();
    }
    if(Reset_Flag == true)
    {
        Reset_Flag = false;
        RE_EC20_Reset();
    }
    return ret_val;
}

void RE_Load_SystemStatus(void)
{
    sys_info.gear_mode = (uint8_t)(GPIOC->IDR & 0x03);
    uint8_t buffer[2] = {0x01, (uint8_t)(GPIOC->IDR & 0x03)};
    /* Tx ODIN restarted */
    RE_CAN_Format_TxMsg(RX_NODE_FRIGG, 00, 0x05, buffer);

    buffer[0] = 0; /* Request Gauge data */
    buffer[1] = 0; /* Request Gauge data */
    uint8_t dock_id = 0;
    RE_CAN_Format_TxMsg(RX_NODE_BATTERY, dock_id, 0x09, buffer);
}

static void RE_ProcessEvents(uint8_t *p_event)
{
    /* p_event = message id */
    uint8_t msg_id = p_event[0];
    uint8_t task[2];
    switch (msg_id)
    {
    case 0x01: /* Gear Pos Change */
        /* TODO: Tx CAN msg to odin */
        break;
    case 0x02: /* LCD Animate */
        RE_LCD_ON();
        HAL_GPIO_WritePin(GPIOC, EC20_IO_Pin, GPIO_PIN_RESET);
        RE_EC20_PowerUp();
        break;
    case 0x03: /* LCD off */
        RE_lcd_power_off();
#if 0
        task[0] = 0x03;
        RE_EnQueue(p_Task_Queue, task, TASK_QUEUE_ITEM_LEN); 
#endif
        break;
    case 0x04: /* Reset EC20 */
#if 0
         task[0] = 0x02;
         RE_EnQueue(p_Task_Queue, task, TASK_QUEUE_ITEM_LEN);
#endif
         break;
    default:
        break;
    }
}

static void RE_ProcessTasks(uint8_t *p_task)
{
    uint8_t msg_id = p_task[0];
    uint8_t * ResetCounter;
    switch (msg_id)
    {
    case 0x01: /* LCD Animate */
        break;
    case 0x02:
#if 0
        RE_EC20_Reset();
        RE_EEPROM_Read(ResetCounter, EC20_RESET_CNT_LEN, EC20_RESET_CNT_ADDR);
        *ResetCounter++;
        RE_EEPROM_Write(ResetCounter, EC20_RESET_CNT_LEN, EC20_RESET_CNT_ADDR);
#endif
        break;
    case 0x03:
#if 0
        uint8_t Data_Buf[13];
        RE_EEPROM_Read(ResetCounter, EC20_RESET_CNT_LEN, EC20_RESET_CNT_ADDR); 
        memset(Data_Buf, 0x0, 13);
        Data_Buf[0] = 0x00;
        Data_Buf[1] = 0xD0;
        Data_Buf[2] = 0x00;
        Data_Buf[3] = 0x02;
        Data_Buf[4] = * ResetCounter;
        RE_EC20_WriteMsg(Data_Buf);
        *ResetCounter = 0x00;
        RE_EEPROM_Write(ResetCounter, EC20_RESET_CNT_LEN, EC20_RESET_CNT_ADDR);
#endif
        HAL_GPIO_WritePin(GPIOC, EC20_IO_Pin, GPIO_PIN_SET);
        break;
    default:
        break;
    }
}